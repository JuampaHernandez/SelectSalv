<?php
require_once 'CRUD.php';
require_once '../ds/DataSource.php';
require_once '../dto/Candidato.php';

class CandidatoDAO implements CRUD
{
    public function mostrar()
    {
        // Instancia para la clase DataSource
        $conn = new DataSource();
        // Verificando estado de la conección
        if (!$conn->conectar())
        {
            return false;
            exit;
        }
        else
        {
            $candidato = null;
            $candidatos = array();
            $sql = 'CALL mostrarCandidatos()';
            if ($stmt = $conn->preparar($sql))
            {
                $stmt->execute();
                // Asignando a la variable el resultado de la consulta
                $stmt->bind_result( $idCan, $nomCan, $nomPar );
                // Enviando valores obtenidos de la consulta
                while ($stmt->fetch())
                {
                    $candidato = new Candidato();
                    $candidato->idCandidato = (int) $idCan;
                    $candidato->nombreCandidato = (string) $nomCan;
                    $candidato->nombrePartido = (string) $nomPar;
                    array_push( $candidatos, (array) $candidato );
                }
                // Desconectando
                $stmt->close();
                $conn->desconectar();
                // Enviando respuesta
                return $candidatos;
                exit;
            }
            else
            {
                // Desconectando
                $conn->desconectar();
                // Enviando respuesta
                return null;
                exit;
            }
        }
    }

    public function agregar($obj)
    {
        // Instancia para la clase DataSource
        $conn = new DataSource();
        // Verificando estado de la conección
        if (!$conn->conectar())
        {
            return false;
            exit;
        }
        else
        {
            $candidato = $obj;
            $sql = "CALL agregarCandidato (?, ?)";
            // Verificando si la consulta se realizó con exito o no
            if ($stmt = $conn->preparar($sql)) {
                $stmt->bind_param("is", $idPartido, $nombreCandidato);
                $idPartido = $candidato->idPartido;
                $nombreCandidato = $candidato->nombreCandidato;
                $stmt->execute();
                // Desconectando
                $stmt->close();
                $conn->desconectar();
                // Enviando respuesta
                return true;
                exit;
            }
            else
            {
                // Desconectando
                $stmt->close();
                $conn->desconectar();
                // Enviando respuesta
                return false;
                exit;
            }
        }
    }

    public function modificar($obj)
    {
        // Instancia para la clase DataSource
        $conn = new DataSource();
        // Verificando estado de la conección
        if (!$conn->conectar())
        {
            return false;
            exit;
        }
        else
        {
            $candidato = $obj;
            $sql = "CALL editarCandidato(?, ?, ?)";
            // Verificando si la consulta se realizó correctamente
            if ($stmt = $conn->preparar($sql))
            {
                $stmt->bind_param('iis', $idCandidato, $idPartido, $nombreCandidato);
                $idCandidato = $candidato->idCandidato;
                $idPartido = $candidato->idPartido;
                $nombreCandidato = $candidato->nombreCandidato;
                $stmt->execute();
                // Desconectando
                $stmt->close();
                $conn->desconectar();
                // Enviando respuesta
                return true;
                exit;
            }
            else
            {
                // Desconectando
                $stmt->close();
                $conn->desconectar();
                // Enviando respuesta
                return false;
                exit;
            }
        }
    }

    public function eliminar($obj)
    {
        // Instancia para la clase DataSource
        $conn = new DataSource();
        // Verificando estado de la conección
        if (!$conn->conectar())
        {
            return false;
            exit;
        }
        else
        {
            $candidato = $obj;
            $sql = "CALL eliminarCandidato(?)";
            // Verificando si la consulta se realizó correctamente
            if ($stmt = $conn->preparar($sql))
            {
                // Enviando parámetros
                $stmt->bind_param('i', $idCandidato);
                $idCandidato = $candidato->idCandidato;
                $stmt->execute();
                // Desconectando
                $stmt->close();
                $conn->desconectar();
                // Enviando respuesta
                return true;
            }
            else
            {
                // Desconectando
                $stmt->close();
                $conn->desconectar();
                // Enviando respuesta
                return false;
            }
        }
    }

    public function buscar($val)
    {
        // Instancia para la clase DataSource
        $conn = new DataSource();
        // Verificando estado de la conección
        if (!$conn->conectar()) {
            return null;
            exit;
        } else {
            $candidato = null;
            $candidatos = array();
            $sql = "SELECT C.Id_Candidato, C.Nombre_Candidato, P.Nombre_Partido FROM Candidato C INNER JOIN Partido P ON C.Id_Partido = P.Id_Partido WHERE C.Nombre_Candidato LIKE '%".$val."%' OR P.Nombre_Partido LIKE '%".$val."%' LIMIT 10";
            if ($stmt = $conn->preparar($sql))
            {
                $stmt->execute();
                // Asignando a la variable el resultado de la consulta
                $stmt->bind_result( $idCan, $nomCan, $nomPar );
                // Enviando valores obtenidos de la consulta
                while ($stmt->fetch())
                {
                    $candidato = new Candidato();
                    $candidato->idCandidato = (int) $idCan;
                    $candidato->nombreCandidato = (string) $nomCan;
                    $candidato->nombrePartido = (string) $nomPar;
                    array_push( $candidatos, (array) $candidato );
                }
                //Desconectando
                $stmt->close();
                $conn->desconectar();
                // Enviando respuesta
                return $candidatos;
                exit;
            }
            else
            {
                //Desconectando
                $conn->desconectar();
                // Enviando respuesta
                return null;
                exit;
            }
        }
    }

    public function llenarSelect1()
    {
        // Instancia para la clase DataSource
        $conn = new DataSource();
        // Verificando estado de la conección
        if (!$conn->conectar()) {
            echo 'Error al conectar: ' . $conn->conectar()->connect_error;
            exit;
        } else {
            $sql = "SELECT Id_Partido, Nombre_Partido FROM Partido;";
            if ($stmt = $conn->preparar($sql)) {
                // Ejecutando la consulta
                $stmt->execute();
                // Obteniendo el resultado de la consulta
                $resultado = $stmt->get_result();
                // Verificando si la consulta devuelve datos
                if ($resultado->num_rows > 0) {
                    // Imprimiendo resultados
                    echo '<option value="0">-Seleccionar-</option>';
                    foreach ($resultado as $indice => $valor) {
                        echo '<option value="' . $valor['Id_Partido'] . '">' . $valor['Nombre_Partido'] . '</option>';
                    }
                }
            } else {
                $stmt->close();
                $conn->desconectar();
                echo 'Error al realizar la consulta: ' . $conn->error();
                exit;
            }
        }
    }

    public function llenarSelect2()
    {
        // Nada que hacer
    }

    public function verificar($obj)
    {
        // Nada que hacer
    }

    public function buscarPersona($val)
    {
        // Nada que hacer
    }
}
