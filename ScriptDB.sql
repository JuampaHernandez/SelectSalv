-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 29-05-2018 a las 10:15:16
-- Versión del servidor: 10.1.28-MariaDB
-- Versión de PHP: 7.1.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `selectsalv`
--
CREATE DATABASE IF NOT EXISTS `selectsalv` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `selectsalv`;

DELIMITER $$
--
-- Procedimientos
--
CREATE DEFINER=`root`@`localhost` PROCEDURE `agregarCandidato` (IN `idPartido` INT, IN `nombreCandidato` VARCHAR(50))  BEGIN
	IF EXISTS (SELECT * FROM partido WHERE partido.Id_Partido = idPartido) THEN
    	INSERT INTO candidato VALUES (NULL, idPartido, nombreCandidato);
    END IF;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `agregarCV` (IN `idMunicipio` INT, IN `nombre` VARCHAR(50), IN `direccion` VARCHAR(100))  NO SQL
INSERT INTO
centro_votacion
VALUES
(NULL, idMunicipio, nombre, direccion)$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `agregarJRV` (IN `idCV` INT, IN `numero` INT)  NO SQL
INSERT INTO
jrv
VALUES
(NULL, idCV, numero)$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `agregarPartido` (IN `nombrePartido` VARCHAR(20))  NO SQL
INSERT INTO partido VALUES (NULL, nombrePartido)$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `agregarUsuario` (IN `passUsuario` VARCHAR(255), IN `idTU` INT, IN `idPersona` INT)  NO SQL
BEGIN
	IF NOT EXISTS (SELECT * FROM usuario WHERE usuario.Id_Persona = idPersona) THEN
		INSERT INTO usuario VALUES (NULL, passUsuario, idTU, idPersona, 		1);
	END IF;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `buscarCandidato` (IN `valor` VARCHAR(50))  NO SQL
SELECT
C.Id_Candidato, C.Nombre_Candidato, P.Nombre_Partido
FROM
Candidato C
INNER JOIN
Partido P
ON
C.Id_Partido = P.Id_Partido
WHERE
C.Nombre_Candidato 
LIKE
"%valor%" 
OR
P.Nombre_Partido
LIKE
@valor
LIMIT 10$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `chartDepartamento` ()  NO SQL
SELECT
D.Nombre_Dep AS nombreDepartamento,
COUNT(D.Id_Dep) AS votos
FROM
voto V
LEFT JOIN
jrv J
ON
V.Id_JRV = J.Id_JRV
LEFT JOIN
centro_votacion CV
ON
J.Id_CV = CV.Id_CV
LEFT JOIN
municipio M
ON
CV.Id_Mun = M.Id_Mun
LEFT JOIN
departamento D
ON
M.Id_Dep = D.Id_Dep
GROUP BY
D.Id_Dep$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `chartPartido` ()  NO SQL
SELECT
P.Nombre_Partido,
COUNT(P.Id_Partido) AS votos
FROM
voto V
INNER JOIN
candidato C
ON
V.Id_Candidato = C.Id_Candidato
INNER JOIN
partido P
ON
C.Id_Partido = P.Id_Partido
GROUP BY
C.Id_Partido$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `editarCandidato` (IN `idCandidato` INT, IN `idPartido` INT, IN `nombreCandidato` VARCHAR(50))  NO SQL
UPDATE
candidato
SET
candidato.Nombre_Candidato = nombreCandidato,
candidato.Id_Partido = idPartido
WHERE
candidato.Id_Candidato = idCandidato$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `editarCV` (IN `id` INT, IN `idMunicipio` INT, IN `nombre` VARCHAR(50), IN `direccion` VARCHAR(100))  NO SQL
UPDATE
centro_votacion
SET
Id_Mun = idMunicipio, Nombre_CV = nombre, Direccion_CV = direccion 
WHERE
Id_CV = id$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `editarJRV` (IN `id` INT, IN `idCV` INT, IN `numero` INT)  NO SQL
UPDATE
jrv
SET
Id_CV = idCV, Numero_JRV = numero
WHERE
Id_JRV = id$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `editarPartido` (IN `idPartido` INT, IN `nombrePartido` VARCHAR(20))  NO SQL
UPDATE
	partido
SET
	partido.Nombre_Partido = nombrePartido
WHERE 
	partido.Id_Partido = idPartido$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `editarUsuario` (IN `idUsuario` INT, IN `idTU` INT, IN `idEVU` INT, IN `passUsuario` VARCHAR(255))  NO SQL
UPDATE usuario
SET
usuario.Id_TU = idTU,
usuario.Id_EVU = idEVU,
usuario.Password_Usuario = passUsuario
WHERE
usuario.Id_Usuario = idUsuario$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `eliminarCandidato` (IN `idCandidato` INT)  NO SQL
DELETE
FROM
candidato
WHERE
candidato.Id_Candidato = idCandidato$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `eliminarCV` (IN `id` INT)  NO SQL
DELETE
FROM
centro_votacion
WHERE
centro_votacion.Id_CV = id$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `eliminarJRV` (IN `id` INT)  NO SQL
DELETE
FROM
jrv
WHERE
Id_JRV = id$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `eliminarPartido` (IN `idPartido` INT)  NO SQL
BEGIN
	IF NOT EXISTS (SELECT * FROM candidato WHERE candidato.Id_Partido = idPartido)
    THEN
    	DELETE FROM partido WHERE partido.Id_Partido = idPartido;
    END IF;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `eliminarUsuario` (IN `idUsuario` INT)  NO SQL
DELETE
FROM
usuario
WHERE
usuario.Id_Usuario = idUsuario$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `mostrarCandidatos` ()  NO SQL
SELECT 
C.Id_Candidato, C.Nombre_Candidato, P.Nombre_Partido 
FROM 
Candidato C 
INNER JOIN 
Partido P 
ON 
C.Id_Partido = P.Id_Partido 
ORDER BY 
C.Id_Candidato 
DESC 
LIMIT 20$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `mostrarCV` ()  NO SQL
SELECT
CV.Id_CV, CV.Nombre_CV, CV.Direccion_CV, M.Nombre_Mun
FROM
centro_votacion CV
INNER JOIN
municipio M
ON
CV.Id_Mun = M.Id_Mun
ORDER BY CV.Id_CV
DESC
LIMIT 20$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `mostrarJRV` ()  NO SQL
SELECT
JRV.Id_JRV, JRV.Numero_JRV, CV.Nombre_CV
FROM
jrv JRV
INNER JOIN
centro_votacion CV
ON
JRV.Id_CV = CV.Id_CV
ORDER BY
JRV.Id_JRV
DESC
LIMIT 20$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `mostrarPartidos` ()  NO SQL
SELECT
	*
FROM
	partido
ORDER BY
	partido.Id_Partido
DESC
LIMIT 20$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `mostrarUsuarios` ()  NO SQL
SELECT 
U.Id_Usuario, P.DUI_Persona, P.Nombre_Persona, P.Apellido_Persona, TU.Descripcion_TU, EVU.Descripcion_EVU
FROM 
usuario U 
INNER JOIN 
persona P
ON
U.Id_Persona = P.Id_Persona
INNER JOIN
tipo_usuario TU
ON
U.Id_TU = TU.Id_TU
INNER JOIN
estado_voto_usuario EVU
ON
U.Id_EVU = EVU.Id_EVU
ORDER BY
U.Id_Usuario
DESC
LIMIT 20$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `reportePartido` ()  NO SQL
SELECT
P.Nombre_Partido,
C.Nombre_Candidato,
COUNT(P.Id_Partido) AS votos
FROM
voto V
INNER JOIN
candidato C
ON
V.Id_Candidato = C.Id_Candidato
INNER JOIN
partido P
ON
C.Id_Partido = P.Id_Partido
GROUP BY
C.Id_Partido$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `seleccionarJRV` ()  NO SQL
SELECT
JRV.Id_JRV, JRV.Numero_JRV, CV.Nombre_CV
FROM
jrv JRV
INNER JOIN
centro_votacion CV
ON
JRV.Id_CV = CV.Id_CV
ORDER BY
JRV.Id_JRV
ASC$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `seleccionarPersona` (IN `duiPersona` VARCHAR(10))  NO SQL
SELECT
P.Id_Persona, P.Nombre_Persona, P.Apellido_Persona
FROM
persona P
WHERE
P.DUI_Persona = duiPersona
LIMIT 1$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `verificarUsuario` (IN `duiPersona` VARCHAR(20))  NO SQL
SELECT
U.Id_Usuario, U.Id_TU, U.Id_EVU, P.Nombre_Persona, U.Password_Usuario
FROM
Usuario U
INNER JOIN
Persona P
ON
U.Id_Persona = P.Id_Persona
WHERE
P.DUI_Persona = duiPersona
LIMIT 1$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `candidato`
--

CREATE TABLE IF NOT EXISTS `candidato` (
  `Id_Candidato` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `Id_Partido` int(10) UNSIGNED NOT NULL,
  `Nombre_Candidato` varchar(50) NOT NULL,
  PRIMARY KEY (`Id_Candidato`),
  KEY `Id_Partido` (`Id_Partido`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `candidato`
--

INSERT INTO `candidato` (`Id_Candidato`, `Id_Partido`, `Nombre_Candidato`) VALUES
(19, 2, 'Juan Pablo'),
(21, 6, 'Verónica Martínez'),
(24, 3, 'William Josué'),
(25, 7, 'Rachel Flores');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `centro_votacion`
--

CREATE TABLE IF NOT EXISTS `centro_votacion` (
  `Id_CV` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `Id_Mun` int(10) UNSIGNED NOT NULL,
  `Nombre_CV` varchar(50) NOT NULL,
  `Direccion_CV` varchar(100) NOT NULL,
  PRIMARY KEY (`Id_CV`),
  KEY `Id_Mun` (`Id_Mun`)
) ENGINE=InnoDB AUTO_INCREMENT=136 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `centro_votacion`
--

INSERT INTO `centro_votacion` (`Id_CV`, `Id_Mun`, `Nombre_CV`, `Direccion_CV`) VALUES
(1, 1, 'Centro Escolar Apaneca', 'Avenida Monseñor Romero y Final Calle 5 de Noviembre entre 21ª y 23ª Calle Oriente, una cuadra al no'),
(2, 2, 'Centro Escolar Atiquizaya', 'Colonia Buenos Aires 3, Diagonal Centroamérica, Avenida Alvarado, contiguo al Ministerio de Hacienda'),
(3, 3, 'Centro Escolar de Ataco', '1ª Calle Poniente entre 60ª Avenida Norte y Boulevard Constitución No. 3549'),
(4, 4, 'Centro Escolar El Refugio', 'Colonia San Francisco, Avenida Las Camelias y Calle Los Abetos No. 21'),
(5, 5, 'Centro Escolar Guaymango', '10ª Avenida Sur y Calle Lara No. 934, Barrio San Jacinto'),
(6, 6, 'Centro Escolar Jujutla', 'Avenida Independencia y Alameda Juan Pablo II, No. 437'),
(7, 7, 'Centro Escolar Menendez', 'AV. INDEPENDENCIA SUR. ENTE 7A. Y 9A. CALLE PTE.  NO. 40'),
(8, 8, 'Centro Escolar San Lorenzo', '2° AVE. NTE. Y 4° CALLE PTE.'),
(9, 9, 'Centro Escolar Tacuba', 'AVENIDA CLUB DE LEONES NO 1 ENTRE 1A CALLE PTEY CALLE RAMON FLORES PONIENTE CHALCHUAPA'),
(10, 10, 'Centro Escolar Acajutla', 'AV. EL CONGO LOTIFICACIÓN EL PROGRESO NO. 6'),
(11, 11, 'Centro Escolar Armenia', '2A. AV. SUR NO. 9, ENTRE 2A C. OTE Y C. TAMARINDO, BO, SANTA CRUZ'),
(12, 12, 'Centro Escolar Caluco', 'CALLE PRINCIPAL, EDIFICIO DE ALCALDIA MUNICIPAL'),
(13, 13, 'Centro Escolar Izalco', '3A AVENIDA SUR ENTRE 2A Y 4A CALLE PONIENTE S/N BO SAN ESTEBAN TEXISTEPEQUE'),
(14, 14, 'Centro Escolar Juayua', '1RA AV SUR Y 1RA CALLE ORIENTE # 2-2'),
(15, 15, 'Centro Escolar Nahuizalco', 'AVENIDA 15 DE ABRIL Y 3A CALLE ORIENTE BARRIO SANTIAGO'),
(16, 16, 'Centro Escolar Nahulingo', '4° CALLE OTE. Y 2° AVE. SUR #8'),
(17, 17, 'Centro Escolar Salcoatitan', 'CALLE CENTRAL CORNELIO AZENON SIERRA BO EL CENTRO 1-59 calle CALLE CENTRAL PONIENTE'),
(18, 18, 'Centro Escolar del Monte', 'AV. CENTRAL NORTE Y 4TA CALLE ORIENTE CARA SUCIA'),
(19, 19, 'Centro Escolar San Julian', 'BARRIO EL CENTRO AVE. No. 1 FTE. A IGLESIA, ALCALDIA MUNICIPAL	'),
(20, 20, 'Centro Escolar Santa Catarina Masahuat', 'CALLE PRINCIPAL NORTE # 1 -2'),
(21, 21, 'Centro Escolar Sonsonate', 'CALLE PRINCIPAL COLONIA SAN ANTONIO SIN NÚMERO'),
(22, 22, 'Centro Escolar de Cuscatlan', 'AVENIDA FERMÍN PINEDA, BARRIO EL CENTRO, FRENTE A LA PNC'),
(23, 23, 'Centro Escolar Chiltiupan', 'BARRIO SAN PABLO ALCALDÍA MUNICIPAL'),
(24, 24, 'Centro Escolar Ciudad Arce', 'BARRIO EL CENTRO FRENTE AL PARQUE'),
(25, 25, 'Centro Escolar Colon', '1ª AVENIDA NORTE Nº 3-1'),
(26, 25, 'Centro Escolar Comasagua', 'COL. ACAXUAL, CALLE PRINCIPAL Y AVENIDA PEDRO DE ALVARADO'),
(27, 27, 'Centro Escolar Huizucar', '1ª AV. NORTE Nº 12, BARRIO EL CENTRO'),
(28, 28, 'Centro Escolar Jayaque', '2DA CALLE ORIENTE Nº 2, BARRIO ASUNCIÓN'),
(29, 29, 'Centro Escolar La libertad', 'AV. DANIEL CORDÓN SUR Nº 2-1'),
(30, 30, 'Centro Escolar Santa Tecla', 'KM. 91 CANTÓN METALÍO, CONTIGUO A PNC, ACAJUTLA, SONSONATE'),
(31, 31, 'Centro Escolar Nuevo Cuscatlan', '2A C. OTE BO EL CALVARIO SAN ANTONIO DEL MONTE'),
(32, 32, 'Centro Escolar San Juan Opico', '2DA AVENIDA NORTE, BARRIO EL CALVARIO'),
(33, 33, 'Centro Escolar Quezaltepeque', '1A.AVENIDA SUR NO.2-7, SANTA TECLA'),
(34, 34, 'Centro Escolar Sacacoyo', '2DA AVENIDA SUR N 4-A BARRIO CENTRO CIUDAD ARCE LA LIBERTAD'),
(35, 35, 'Centro Escolar Talnique', 'CANTÓN ATEOS CARRETERA A SONSONATE, FRENTE A FERRETERÍA EL PINO'),
(36, 36, 'Centro Escolar Aguilares', 'CENTRO COMERCIAL LAS PALMERAS 4° CALLE PTE, #1 – 7,  local  #  9'),
(37, 37, 'Centro Escolar Apopa', 'LOCAL No. 2 DEL ALA “A” PRIMER NIVEL, EDIFICIO ADMINISTRATIVO DEL COMPLEJO TURÍSTICO DEL PUERTO DE L'),
(38, 38, 'Centro Escolar Ayutuxtepeque', 'COLONIA  SITIO DEL NIÑO 2A. CALLE CASA NO.10'),
(39, 39, 'Centro Escolar Ciudad Delgado', '1ª AV. NTE. Y CALLE EMILIA MERCHER N°1, BARRIO CONCEPCIÓN, QUEZALTEPEQUE'),
(40, 40, 'Centro Escolar Cuscatancingo', '1ª CALLE PONIENTE N. 11, ANTIGUO CUSCATLÁN'),
(41, 41, 'Centro Escolar El Paisnal', 'BO SAN JOSE,2A AV SUR Y CALLE GAMBOA #2 JAYAQUE, LA LIBERTAD'),
(42, 42, 'Centro Escolar Guazapa', '3a AVE. NORTE No. 4, BARRIO EL CENTRO, SAN JUAN OPICO, LA LIBERTAD'),
(43, 43, 'Centro Escolar Ilopango', 'CNTRO.COMERCIAL PLAZA MERLIOT, LOCAL 3015 3ER. NIVEL'),
(44, 44, 'Centro Escolar Mejicanos', '1ª AV. NTE. Y CALLE OSCAR A. ROMERO LOCAL NO.2,  ZARAGOZA'),
(45, 45, 'Centro Escolar Nejapa', 'CENTRO COMERCIAL MULTIPLAZA 1ER. NIVEL LOCAL 27-B, ANTIGUO CUSCATLÁN'),
(46, 46, 'Centro Escolar Panchimalco', 'CALLE DOROTEO VASCONCELO, N° 30 A BARRIO SANTA BARBARA'),
(47, 47, 'Centro Escolar Rosario de Mora', '1a AVE. SUR, BARRIO EL CALVARIO, CASA No. 10, ILOBASCO, CABAÑAS'),
(48, 48, 'Centro Escolar San Marcos', 'BARRIO CALVARIO CALLE CHAPELCORO CASA # 4 CIUDAD DOLORES'),
(49, 49, 'Centro Escolar San Martin', 'FINAL 1A.CALLE OTE, BO. EL CENTRO'),
(50, 50, 'Centro Escolar San Salvador', '4° AV. SUR, CASA N° 4, BARRIO EL CENTRO'),
(51, 51, 'Centro Escolar Soyapango', 'CALLE EL COMERCIO BO EL CENTRO CONTIGUO AL MERCADO'),
(52, 52, 'Centro Escolar Candelaria de la Frontera', 'AVENIDA  LIBERTAD,  CENTRO DE GOBIERNO, CHALATENANGO'),
(53, 53, 'Centro Escolar Chalchuapa', 'CARRETERA TRONCAL DEL NORTE KM 48 1/2 FRENTE A PLAZA COMERCIAL DON YON'),
(54, 54, 'Centro Escolar Coatepeque', 'BARRIO EL CENTRO AGUA CALIENTE'),
(55, 55, 'Centro Escolar El Congo', 'BARRIO EL CENTRO, LOCAL ALCALDÍA MUNICIPAL, CONTIGUO A CASA DE LA CULTURA'),
(56, 56, 'Centro Escolar El Porvenir', 'BARRIO EL CENTRO, CALLE PRINCIPAL , LOCAL ALCALDÍA MUNICIPAL'),
(57, 57, 'Centro Escolar Masahuat', 'BARRIO EL CENTRO , ALCALDÍA MUNICIPAL'),
(58, 58, 'Centro Escolar Metapan', 'BARRIO EL CENTRO, PRIMERA CALLE PONIENTE, CONTIGUO A CASA DE LA CULTURA.'),
(59, 59, 'Centro Escolar San Antonio Pajonal', 'BARRIO EL CENTRO'),
(60, 60, 'Centro Escolar Santa Ana', 'CASA COMUNAL COL. REUBICACIÓN DOS, poligono 2 casa comunal CANTÓN SAN BARTOLO, CHALATENANGO'),
(61, 61, 'Centro Escolar Santiago de la Frontera', '6° CALLE PTE. # 1310 FRENTE A PARQUE MUNICIPAL BO. EL CENTRO'),
(62, 62, 'Centro Escolar Texistepeque', 'BARRIO EL CENTRO, LOCAL ALCALDÍA MUNICIPAL'),
(63, 63, 'Centro Escolar Agua Caliente', 'LOCAL ALC. MUNICIPAL BO. EL CENTRO, FTE. PLAZA PÚBLICA'),
(64, 64, 'Centro Escolar Arcatao', 'CALLE PPAL.,BARRIO EL CENTRO, CASA N° 6,  TEJUTLA'),
(65, 65, 'Centro Escolar Azacualpa', 'BARRIO EL CENTRO, CALLE PRINCIPAL A 25 MTRS. DE ALCALDÍA MUNICIPAL'),
(66, 66, 'Centro Escolar Chalatenango', 'ALCALDIA LA LAGUNA CHALATENANGO'),
(67, 67, 'Centro Escolar Citala', 'BO. SAN NICOLAS 3A AVE. SUR #3'),
(68, 68, 'Centro Escolar Comalapa', '3A. CALLE PONIENTE # 7. BO EL CENTRO, SAN BARTOLOME PERULAPIA'),
(69, 69, 'Centro Escolar Dulce Nombre de Maria', 'BARRIO EL CALVARIO CONTIGUO AL PARQUE DE ENTRADA SAN JOSE GUAYABAL CUSCATLAN'),
(70, 70, 'Centro Escolar El Carrizal', 'BO. EL CENTRO – ALCALDÍA MUNICIPAL – SANTA CRUZ ANALQUITO'),
(71, 71, 'Centro Escolar El Paraiso', '2a AVE. SUR, No. 7 PORCIÓN DE PREDIO URBANO, BARRIO EL CALVARIO, SUCHITOTO, CUSCATLÁN'),
(72, 72, 'Centro Escolar La Laguna', 'BO. EL CENTRO – ALCALDÍA MUNICIPAL –  EL ROSARIO'),
(73, 73, 'Centro Escolar Ojos de Agua', 'BO. EL CENTRO  3A. CALLE PONIENTE, CONTIGUO A ALCALDÍA MUNICIPAL'),
(74, 74, 'Centro Escolar Tejutla', 'AV. GRAL. CABAÑA C.D.GOB.'),
(75, 75, 'Centro Escolar Candelaria', '3° AV. NORTE BARRIO EL CENTRO'),
(76, 76, 'Centro Escolar Cojutepeque', 'CALLE  AMELIA UMANZOR, BARRIO EL CENTRO'),
(77, 77, 'Centro Escolar El Rosario', 'BARRIO EL CENTRO, ALCALDÍA MUNICIPAL, FRENTE A UNIDAD DE SALUD  EL CARMEN'),
(78, 78, 'Centro Escolar Monte San Juan', 'BARRIO EL CENTRO, CALLE PRINCIPAL'),
(79, 79, 'Centro Escolar San Cristobal', 'BO. EL CENTRO, CONTIGUO A IGLESIA CATÓLICA'),
(80, 80, 'Centro Escolar San Ramon', 'BARRIO SAN FRANCISCO, FRENTE  A ALCALDIA'),
(81, 81, 'Centro Escolar Suchitoto', 'CANTÓN MONTECA CASERIO LA PISTA, NUEVA ESPARTA'),
(82, 82, 'Centro Escolar Tenancingo', 'ALLE ESCALON, BARRIO EL CENTRO'),
(83, 83, 'Centro Escolar Cuyultitan', 'BARRIO EL CENTRO'),
(84, 84, 'Centro Escolar El Rosario de la Paz', 'CALLE DANIEL ARIAS , FRENTE AL PARQUE'),
(85, 85, 'Centro Escolar Olocuilta', 'CALLE PRINCIPAL'),
(86, 86, 'Centro Escolar Paraiso de Osorio', 'CALLE AL CEMENTERIO, BARRIO LAS DELICIAS, CALLE LA QUESERA  SANTA ROSA DE LIMA'),
(87, 87, 'Centro Escolar San Antonio Masahuat', 'AVENIDA URZULO MARQUEZ, BARRIO EL CENTRO'),
(88, 88, 'Centro Escolar San Francisco Chinameca', 'CENTRO COMERCIAL PLAZA SOYAPANGO BLVD. DEL EJERCITO KM 4 LOCAL ANCLA C-1'),
(89, 89, 'Centro Escolar San Juan Nonualco', '2A. CALLE PTE. Y 3° AVE SUR BO. EL CENTRO, CENTRO DE GOBIERNO'),
(90, 90, 'Centro Escolar San Juan Talpa', 'CALLE PRINCIPAL BO. EL CALVARIO'),
(91, 91, 'Centro Escolar Santiago Nonualco', 'BARRIO LA ALIANZA, CALLE PRINCIPAL # 4, CORINTO'),
(92, 92, 'Centro Escolar Tapalhuaca', 'BO. EL CENTRO FRENTE AL PARQUE, ANEXO A CASA DE LA CULTURA'),
(93, 93, 'Centro Escolar Zacatecoluca', 'AV. JULIO VENTURA, #2 BARRIO EL CENTRO'),
(94, 94, 'Centro Escolar Apastepeque', 'BO. EL CENTRO, DENTRO DE LAS INSTALACIONES DE LA ALCALDIA MUNICIPAL'),
(95, 95, 'Centro Escolar Guadalupe', 'BO. EL CENTRO FRENTE AL PARQUE, EN LA ALCALDÍA'),
(96, 96, 'Centro Escolar San Lorenzo', '1A. CALLE OTE. Y 1A. AVENIDA NORTE BO. EL CENTRO'),
(97, 97, 'Centro Escolar San Sebastian', 'BARRIO EL CALVARIO, AV. SIMEON CAÑAS N° 407 B  FRENTE AL MINISTERIO DE TRABAJO'),
(98, 98, 'Centro Escolar San Vicente', '2A. AV. NRTE.  BARRIO SANTA LUCIA,  CHAPELTIQUE'),
(99, 99, 'Centro Escolar Santa Clara', 'CALLE JESUS APARICIO BARRIO EL CENTRO CHIRILAGUA CONTIGUO A LA NEVERIA'),
(100, 100, 'Centro Escolar Santo Domingo', 'B° ROMA 4° CALLE OTE. Y 2° AV. SUR N°3'),
(101, 101, 'Centro Escolar Tecoluca', '3°CALLE OTE. LOCAL 5 B° LA CRUZ'),
(102, 102, 'Centro Escolar Verapaz', 'CALLE PRINCIPAL BARRIO SAN LUIS NUEVA GUADALUPE'),
(103, 103, 'Centro Escolar Cinquera', 'CARRETERA PANAMERICANA CANTÓN SAN ANTONIO SILVA KM. 158 ½ '),
(104, 104, 'Centro Escolar Dolores', 'CALLE FEDERICO PENADO # 9'),
(105, 105, 'Centro Escolar Ilobasco', '3° AV. SUR B° EL CENTRO # 16 BERLIN'),
(106, 106, 'Centro Escolar Jutiapa', '2º CALLE ORIENTE Bº EL CALVARIO, CIUDAD EL TRIUNFO'),
(107, 107, 'Centro Escolar San Isidro', 'CALLE 14 DE DICIEMBRE # 2,BO EL CENTRO  JIQUILISCO'),
(108, 108, 'Centro Escolar Sensuntepeque', 'CALLE FRANCISCO GAVIDIA Nº 13 Bº EL CENTRO, JUCUAPA'),
(109, 109, 'Centro Escolar Tejutepeque', '1RA CALLE PTE, CALLE PPAL. ½  CUADRA AL PTE DE LA ALCALDIA   Bº LA PARROQUIA, JUCUARAN'),
(110, 110, 'Centro Escolar Victoria', 'CALLE PPAL , BARRIO CONCEPCION,SALIDA A BERLIN, MERCEDES UMAÑA'),
(111, 111, 'Centro Escolar Usulutan', 'AV. FRANCISCO OSEGUEDA, B° EL CENTRO CONTIGUO A ALCALDÍA MUNICIPAL, FRENTE A LA PLACITA MUNICIPAL'),
(112, 112, 'Centro Escolar Tecapan', '5TA AVE SUR Y 1RA CALLE PTE BO EL CALVARIO  CASA 2 SANTA ELENA'),
(113, 113, 'Centro Escolar Santiago de Maria', 'AV. 15 DE SEPTIEMBRE, FTE. A POLLO CAMPERO SANTIAGO DE MARIA'),
(114, 114, 'Centro Escolar Santa Maria', '4ª AV. SUR, Y 9 CALLE OTE ½ ANTES DEL CENTRO DE GOBIERNO, USULUTÁN'),
(115, 115, 'Centro Escolar California', '3A. AVENIDA NORTE Nº 22, Bº EL CALVARIO'),
(116, 116, 'Centro Escolar Berlin', 'AV.25 DE JULIO CASA Nº1 BARRIO EL CENTRO CALLE DARIO LOPEZ, APASTEPEQUE'),
(117, 117, 'Centro Escolar Alegria', 'AVENIDA MARCELINO GARCIA FLAMENCO BARRIO EL CENTRO, CONTIGUO A NEVERIA SAN ESTEBAN CATARINA'),
(118, 118, 'Centro Escolar Chinameca', 'CALLE PRINCIPAL, VITAN LADISLAO, BARRIO EL CENTRO, SAN ILDEFONSO'),
(119, 119, 'Centro Escolar Ciudad Barrios', 'AV. 14 DE JULIO , BARRIO EL TRANSITO  Nº 8, SAN SEBASTIÁN'),
(120, 120, 'Centro Escolar El Transito', 'AV. 15 DE SEPT. NO. 19, BO. SAN JOSÉ, CALLE AL CEMENTERIO,  SANTO DOMINGO'),
(121, 121, 'Centro Escolar Nueva Guadalupe', 'BARRIO EL CENTRO, CALLE MORAZÁN CONTIGUO A ALCALDÍA MUNICIPAL, TECOLUCA'),
(122, 122, 'Centro Escolar San Miguel', '1A. AV. NORTE NO.5, BO. SAN MIGUELITO, FRENTE PARQUE CENTRAL, COSTADO PTE. ALCALDÍA MUNICIPAL, VERAP'),
(123, 123, 'Centro Escolar San Rafael Oriente', 'CALLE DR NICOLAS PEÑA NO 17 BA EL CENTRO'),
(124, 124, 'Centro Escolar San Fracisco Gotera', 'CALLE GERARDO BARRIOS  N° 60, BARRIO EL CENTRO, COSTADO SUR DE IGLESIA CATOLICA'),
(125, 125, 'Centro Escolar Perquin', 'A ELCENTRO CASA NO 2 CALLE EL MIRADOR'),
(126, 126, 'Centro Escolar Osicala', 'CALLE FRANCISCO MENENDEZ NO 2 BA EL CENTRO'),
(127, 127, 'Centro Escolar Jocoro', 'BA EL CENTRO CALLE PRINCIPAL FRENTE A PARQUE MUNICIPAL'),
(128, 128, 'Centro Escolar El Rosario', 'AV. MORAZÁN, CALLE PPAL. CONTIGUO A ALCALDIA MUNICIPAL, BARRIO EL  CENTRO'),
(129, 129, 'Centro Escolar Corinto', 'FINAL DE LA 1° CALLE PONIENTE Y 3ª AVENIDA NTE. SAN RAFAEL OBRAJUELO'),
(130, 130, 'Centro Escolar Cacaopera', 'CALLE LUIS RIVAS VIDEZ, BARRIO EL  CENTRO, CONTIGUO A ALCALDIA MUNICIPAL'),
(131, 131, 'Centro Escolar Bolivar', 'CALLE LA MASCOTA Y 79 AV. SUR #440 EDIFICIO LAS PLAZAS'),
(132, 132, 'Centro Escolar Conchagua', 'ENTRO COMERCIAL METRÓPOLIS LOCAL No 34, ZACAMIL'),
(133, 133, 'Centro Escolar El Carmen', '4TA. CALLE ORIENTE No 3 BARRIO EL CALVARIO'),
(134, 134, 'Centro Escolar El Sauce', 'ALLE LA GLORIA Y AV. JUAN BERTIS No 1'),
(135, 135, 'Centro Escolar La Union', 'CALLE PRINCIPAL FRENTE A LA IGLESIA PARROQUIA');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `comentario`
--

CREATE TABLE IF NOT EXISTS `comentario` (
  `Id_Comentario` int(10) UNSIGNED NOT NULL,
  `Nombre_Comentarista` varchar(30) NOT NULL,
  `Email_Comentarista` varchar(30) NOT NULL,
  `Contenido` varchar(500) NOT NULL,
  PRIMARY KEY (`Id_Comentario`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `departamento`
--

CREATE TABLE IF NOT EXISTS `departamento` (
  `Id_Dep` int(10) UNSIGNED NOT NULL,
  `Nombre_Dep` varchar(30) NOT NULL,
  PRIMARY KEY (`Id_Dep`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `departamento`
--

INSERT INTO `departamento` (`Id_Dep`, `Nombre_Dep`) VALUES
(1, 'Ahuachapan'),
(2, 'Sonsonate'),
(3, 'La Libertad'),
(4, 'San Salvador'),
(5, 'Santa Ana'),
(6, 'Chalatenango'),
(7, 'Cuscatlan'),
(8, 'La Paz'),
(9, 'San Vicente'),
(10, 'Cabañas'),
(11, 'Usulutan'),
(12, 'San Miguel'),
(13, 'Morazan'),
(14, 'La Unión');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `estado_voto_usuario`
--

CREATE TABLE IF NOT EXISTS `estado_voto_usuario` (
  `Id_EVU` int(10) UNSIGNED NOT NULL,
  `Descripcion_EVU` varchar(20) NOT NULL,
  PRIMARY KEY (`Id_EVU`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `estado_voto_usuario`
--

INSERT INTO `estado_voto_usuario` (`Id_EVU`, `Descripcion_EVU`) VALUES
(1, 'Pendiente'),
(2, 'Realizado');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `jrv`
--

CREATE TABLE IF NOT EXISTS `jrv` (
  `Id_JRV` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `Id_CV` int(10) UNSIGNED NOT NULL,
  `Numero_JRV` int(11) NOT NULL,
  PRIMARY KEY (`Id_JRV`),
  UNIQUE KEY `Numero_JRV` (`Numero_JRV`),
  KEY `Id_CV` (`Id_CV`)
) ENGINE=InnoDB AUTO_INCREMENT=137 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `jrv`
--

INSERT INTO `jrv` (`Id_JRV`, `Id_CV`, `Numero_JRV`) VALUES
(1, 1, 1),
(2, 2, 2),
(3, 3, 3),
(4, 4, 4),
(5, 5, 5),
(6, 6, 6),
(7, 7, 7),
(8, 8, 8),
(9, 9, 9),
(10, 10, 10),
(11, 11, 11),
(12, 12, 12),
(13, 13, 13),
(14, 14, 14),
(15, 15, 15),
(16, 16, 16),
(17, 17, 17),
(18, 18, 18),
(19, 19, 19),
(20, 20, 20),
(21, 21, 21),
(22, 22, 22),
(23, 23, 23),
(24, 24, 24),
(25, 25, 25),
(26, 26, 26),
(27, 27, 27),
(28, 28, 28),
(29, 29, 29),
(30, 30, 30),
(31, 31, 31),
(32, 32, 32),
(33, 33, 33),
(34, 34, 34),
(35, 35, 35),
(36, 36, 36),
(37, 37, 37),
(38, 38, 38),
(39, 39, 39),
(40, 40, 40),
(41, 41, 41),
(42, 42, 42),
(43, 43, 43),
(44, 44, 44),
(45, 45, 45),
(46, 46, 46),
(47, 47, 47),
(48, 48, 48),
(49, 49, 49),
(50, 50, 50),
(51, 51, 51),
(52, 52, 52),
(53, 53, 53),
(54, 54, 54),
(55, 55, 55),
(56, 56, 56),
(57, 57, 57),
(58, 58, 58),
(59, 59, 59),
(60, 60, 60),
(61, 61, 61),
(62, 62, 62),
(63, 63, 63),
(64, 64, 64),
(65, 65, 65),
(66, 66, 66),
(67, 67, 67),
(68, 68, 68),
(69, 69, 69),
(70, 70, 70),
(71, 71, 71),
(72, 72, 72),
(73, 73, 73),
(74, 74, 74),
(75, 75, 75),
(76, 76, 76),
(77, 77, 77),
(78, 78, 78),
(79, 79, 79),
(80, 80, 80),
(81, 81, 81),
(82, 82, 82),
(83, 83, 83),
(84, 84, 84),
(85, 85, 85),
(86, 86, 86),
(87, 87, 87),
(88, 88, 88),
(89, 89, 89),
(90, 90, 90),
(91, 91, 91),
(92, 92, 92),
(93, 93, 93),
(94, 94, 94),
(95, 95, 95),
(96, 96, 96),
(97, 97, 97),
(98, 98, 98),
(99, 99, 99),
(100, 100, 100),
(101, 101, 101),
(102, 102, 102),
(103, 103, 103),
(104, 104, 104),
(105, 105, 105),
(106, 106, 106),
(107, 107, 107),
(108, 108, 108),
(109, 109, 109),
(110, 110, 110),
(111, 111, 111),
(112, 112, 112),
(113, 113, 113),
(114, 114, 114),
(115, 115, 115),
(116, 116, 116),
(117, 117, 117),
(118, 118, 118),
(119, 119, 119),
(120, 120, 120),
(121, 121, 121),
(122, 122, 122),
(123, 123, 123),
(124, 124, 124),
(125, 125, 125),
(126, 126, 126),
(127, 127, 127),
(128, 128, 128),
(129, 129, 129),
(130, 130, 130),
(131, 131, 131),
(132, 132, 132),
(133, 133, 133),
(134, 134, 134),
(135, 135, 135);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `jrv_persona`
--

CREATE TABLE IF NOT EXISTS `jrv_persona` (
  `Id_JRV` int(10) UNSIGNED NOT NULL,
  `Id_Persona` int(10) UNSIGNED NOT NULL,
  PRIMARY KEY (`Id_JRV`,`Id_Persona`),
  UNIQUE KEY `Id_Persona` (`Id_Persona`),
  KEY `Id_JRV` (`Id_JRV`),
  KEY `Id_Persona_2` (`Id_Persona`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `municipio`
--

CREATE TABLE IF NOT EXISTS `municipio` (
  `Id_Mun` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `Id_Dep` int(10) UNSIGNED NOT NULL,
  `Nombre_Mun` varchar(30) NOT NULL,
  PRIMARY KEY (`Id_Mun`),
  KEY `Id_Dep` (`Id_Dep`)
) ENGINE=InnoDB AUTO_INCREMENT=136 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `municipio`
--

INSERT INTO `municipio` (`Id_Mun`, `Id_Dep`, `Nombre_Mun`) VALUES
(1, 1, 'Apaneca'),
(2, 1, 'Atiquizaya'),
(3, 1, 'Concepcion de Ataco'),
(4, 1, 'El Refugio'),
(5, 1, 'Guaymango'),
(6, 1, 'Jujutla'),
(7, 1, 'San Francisco Menendez'),
(8, 1, 'San Lorenzo'),
(9, 1, 'Tacuba'),
(10, 2, 'Acajutla'),
(11, 2, 'Armenia'),
(12, 2, 'Caluco'),
(13, 2, 'Izalco'),
(14, 2, 'Juayua'),
(15, 2, 'Nahuizalco'),
(16, 2, 'Nahulingo'),
(17, 2, 'Salcoatitan'),
(18, 2, 'San Antonio del Monte'),
(19, 2, 'San Julian'),
(20, 2, 'Santa Catarina Masahuat'),
(21, 2, 'Sonsonate'),
(22, 3, 'Antiguo Cuscatlan'),
(23, 3, 'Chiltiupan'),
(24, 3, 'Ciudad Arce'),
(25, 3, 'Colon'),
(26, 3, 'Comasagua'),
(27, 3, 'Huizucar'),
(28, 3, 'Jayaque'),
(29, 3, 'La libertad'),
(30, 3, 'Santa Tecla'),
(31, 3, 'Nuevo Cuscatlan'),
(32, 3, 'San Juan Opico'),
(33, 3, 'Quezaltepeque'),
(34, 3, 'Sacacoyo'),
(35, 3, 'Talnique'),
(36, 4, 'Aguilares'),
(37, 4, 'Apopa'),
(38, 4, 'Ayutuxtepeque'),
(39, 4, 'Ciudad Delgado'),
(40, 4, 'Cuscatancingo'),
(41, 4, 'El Paisnal'),
(42, 4, 'Guazapa'),
(43, 4, 'Ilopango'),
(44, 4, 'Mejicanos'),
(45, 4, 'Nejapa'),
(46, 4, 'Panchimalco'),
(47, 4, 'Rosario de Mora'),
(48, 4, 'San Marcos'),
(49, 4, 'San Martin'),
(50, 4, 'San Salvador'),
(51, 4, 'Soyapango'),
(52, 5, 'Candelaria de la Frontera'),
(53, 5, 'Chalchuapa'),
(54, 5, 'Coatepeque'),
(55, 5, 'El Congo'),
(56, 5, 'El Porvenir'),
(57, 5, 'Masahuat'),
(58, 5, 'Metapan'),
(59, 5, 'San Antonio Pajonal'),
(60, 5, 'Santa Ana'),
(61, 5, 'Santiago de la Frontera'),
(62, 5, 'Texistepeque'),
(63, 6, 'Agua Caliente'),
(64, 6, 'Arcatao'),
(65, 6, 'Azacualpa'),
(66, 6, 'Chalatenango'),
(67, 6, 'Citala'),
(68, 6, 'Comalapa'),
(69, 6, 'Dulce Nombre de Maria'),
(70, 6, 'El Carrizal'),
(71, 6, 'El Paraiso'),
(72, 6, 'La Laguna'),
(73, 6, 'Ojos de Agua'),
(74, 6, 'Tejutla'),
(75, 7, 'Candelaria'),
(76, 7, 'Cojutepeque'),
(77, 7, 'El Rosario'),
(78, 7, 'Monte San Juan'),
(79, 7, 'San Cristobal'),
(80, 7, 'San Ramon'),
(81, 7, 'Suchitoto'),
(82, 7, 'Tenancingo'),
(83, 8, 'Cuyultitan'),
(84, 8, 'El Rosario de la Paz'),
(85, 8, 'Olocuilta'),
(86, 8, 'Paraiso de Osorio'),
(87, 8, 'San Antonio Masahuat'),
(88, 8, 'San Francisco Chinameca'),
(89, 8, 'San Juan Nonualco'),
(90, 8, 'San Juan Talpa'),
(91, 8, 'Santiago Nonualco'),
(92, 8, 'Tapalhuaca'),
(93, 8, 'Zacatecoluca'),
(94, 9, 'Apastepeque'),
(95, 9, 'Guadalupe'),
(96, 9, 'San Lorenzo'),
(97, 9, 'San Sebastian'),
(98, 9, 'San Vicente'),
(99, 9, 'Santa Clara'),
(100, 9, 'Santo Domingo'),
(101, 9, 'Tecoluca'),
(102, 9, 'Verapaz'),
(103, 10, 'Cinquera'),
(104, 10, 'Dolores'),
(105, 10, 'Ilobasco'),
(106, 10, 'Jutiapa'),
(107, 10, 'San Isidro'),
(108, 10, 'Sensuntepeque'),
(109, 10, 'Tejutepeque'),
(110, 10, 'Victoria'),
(111, 11, 'Usulutan'),
(112, 11, 'Tecapan'),
(113, 11, 'Santiago de Maria'),
(114, 11, 'Santa Maria'),
(115, 11, 'California'),
(116, 11, 'Berlin'),
(117, 11, 'Alegria'),
(118, 12, 'Chinameca'),
(119, 12, 'Ciudad Barrios'),
(120, 12, 'El Transito'),
(121, 12, 'Nueva Guadalupe'),
(122, 12, 'San Miguel'),
(123, 12, 'San Rafael Oriente'),
(124, 13, 'San Francisco Gotera'),
(125, 13, 'Perquin'),
(126, 13, 'Osicala'),
(127, 13, 'Jocoro'),
(128, 13, 'El Rosario'),
(129, 13, 'Corinto'),
(130, 13, 'Cacaopera'),
(131, 14, 'Bolivar'),
(132, 14, 'Conchagua'),
(133, 14, 'El Carmen'),
(134, 14, 'El Sauce'),
(135, 14, 'La Unión');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `partido`
--

CREATE TABLE IF NOT EXISTS `partido` (
  `Id_Partido` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `Nombre_Partido` varchar(20) NOT NULL,
  PRIMARY KEY (`Id_Partido`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `partido`
--

INSERT INTO `partido` (`Id_Partido`, `Nombre_Partido`) VALUES
(1, 'ARENA'),
(2, 'FMLN'),
(3, 'Nuevas Ideas'),
(6, 'PCN'),
(7, 'PDC');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `persona`
--

CREATE TABLE IF NOT EXISTS `persona` (
  `Id_Persona` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `Nombre_Persona` varchar(20) NOT NULL,
  `Apellido_Persona` varchar(20) NOT NULL,
  `Edad_Persona` int(11) NOT NULL,
  `DUI_Persona` varchar(10) NOT NULL,
  `Direccion` varchar(100) NOT NULL,
  PRIMARY KEY (`Id_Persona`),
  UNIQUE KEY `DUI_Persona` (`DUI_Persona`)
) ENGINE=InnoDB AUTO_INCREMENT=152 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `persona`
--

INSERT INTO `persona` (`Id_Persona`, `Nombre_Persona`, `Apellido_Persona`, `Edad_Persona`, `DUI_Persona`, `Direccion`) VALUES
(1, 'Juan Pablo', 'Elias Hernandez', 18, '12345678-9', ''),
(2, 'Verónica Rachel', 'Martínez Flores', 18, '98765432-1', ''),
(3, 'William Josue', 'Elias Hernández', 18, '12121212-1', 'Ejemplo de dirección.'),
(4, 'María Mireya', 'Acevedo Manríquez', 44, '31351355-0', 'Apaneca'),
(5, 'Enrique', 'Acevedo Mejía', 22, '14541135-0', 'Ahuachapan'),
(6, 'Tomás José', 'Acosta Canto', 33, '66663635-0', 'Sonsonate'),
(7, 'Irma', 'Aguilar Dorantes', 55, '16541135-0', 'Comasagua'),
(8, 'María Ofelia', 'Aguilar Lemus', 40, '25725777-0', 'Colon'),
(9, 'Marcela', 'Aguilar Loranca', 66, '86838558-0', 'San Julian'),
(10, 'Fredy Francisco', 'Aguilar Pérez', 21, '57322666-0', 'San Salvador'),
(11, 'Salomón', 'Alarcón Licona', 52, '73868285-0', 'La Union'),
(12, 'José Genaro', 'Alarcón López', 36, '86257257-0', 'Atiquizaya'),
(13, 'Hipólito', 'Alatriste Pérez', 43, '68368278-0', 'Apaneca'),
(14, 'José Israel', 'Alcántar Camacho', 44, '73575277-0', 'Sonsonate'),
(15, 'Jacinta Guillermina', 'Alderete Porras', 32, '75272588-0', 'Chiltiupan'),
(16, 'Adolfo', 'Aldrete Vargas', 28, '52825858-0', 'San Lorenzo'),
(17, 'Víctor Hugo', 'Alejo Guerrero', 37, '86368369-0', 'Nuevo Cuscatlan'),
(18, 'María del Rosario', 'Alemán Mundo', 78, '36836886-0', 'Antiguo Cuscatlan'),
(19, 'Matilde', 'Basaldúa Ramírez', 92, '33585388-0', 'Nuevo Cuscatlan'),
(20, 'Gregorio', 'Benítez Ferrusquía', 66, '38963999-0', 'San Salvador'),
(21, 'José Antonio', 'Bermúdez Manrique', 23, '36937545-0', 'Candelaria de la Frontera'),
(22, 'Gigliola Taide', 'Bernal Rosales', 27, '24624474-0', 'Apopa'),
(23, 'Miguel Ángel', 'Betancourt Vázquez', 77, '24741246-0', 'Soyapango'),
(24, 'Noel', 'Betanzos Torres', 55, '74242464-0', 'Mejicanos'),
(25, 'María de Lourdes', 'Campos Campos', 64, '42620756-0', 'Coatepeque'),
(26, 'Jorge Alonso', 'Campos Saito', 38, '64242664-0', 'La Laguna'),
(27, 'Marco Antonio', 'Cárdenas Cornejo', 55, '46246246-0', 'El Rosario'),
(28, 'Marcos', 'Cardona Salazar', 22, '24624664-0', 'El Rosario'),
(29, 'Agustín', 'Carreño Chapa', 77, '24662442-0', 'Ojos de Agua'),
(30, 'Mayra del Carmen', 'Carrillo Trujillo', 37, '24642664-0', 'Cojutepeque'),
(31, 'Roque José', 'Castilla Santana', 55, '42524642-0', 'Candelaria'),
(32, 'Enrique Alfonso', 'Castillo López', 33, '65375224-0', 'El Rosario'),
(33, 'Félix Arturo', 'Castillo Ramírez', 28, '24624624-0', 'Usulutan'),
(34, 'Manuel Augusto', 'Castro López', 75, '69068086-0', 'San Vicente'),
(35, 'Julio César ', 'Díaz Morfín', 43, '68468469-0', 'San lorenzo'),
(36, 'Julio Eduardo', 'Díaz Sánchez', 54, '94694996-0', 'San Isidro'),
(37, 'Gabriel', 'Domínguez Barrios', 77, '46146142-0', 'San Juan Talpa'),
(38, 'Miguel Ángel', 'Domínguez Velasco', 71, '42657264-0', 'Colon'),
(39, 'Carlos Rafael', 'Durán Suárez', 65, '74165247-0', 'Santa Clara'),
(40, 'Francisco Enrique', 'Escobar Beltrán', 55, '24624246-0', 'Santo Domingo'),
(41, 'Francisco Octavio', 'Escudero Contreras', 67, '47246462-0', 'California'),
(42, 'Carlos', 'Esquivel Estrada', 22, '24246464-0', 'Alegria'),
(43, 'Jesús Arcadio', 'Félix Brasil', 21, '24724262-0', 'Berlin'),
(44, 'Castillo Raúl', 'Fernández', 18, '24641664-0', 'Ciudad Barrios'),
(45, 'María Enriqueta', 'Fernández Haggar', 25, '61414614-0', 'Guadalupe'),
(46, 'Ángel Virgilio', 'Ferreira Centeno', 18, '42657254-0', 'Conchagua'),
(47, 'Mario Fernando', 'Gallegos León', 29, '24724642-0', 'Bolivar'),
(48, 'Martín Ángel', 'Gamboa Banda', 29, '56264422-0', 'Ilobasco'),
(49, 'Ricardo', 'García Chávez', 18, '61353742-0', 'Chinameca'),
(50, 'Mario Alberto', 'García García', 18, '26435727-0', 'El Carmen'),
(51, 'Miriam Aidé', 'García González', 36, '46276424-0', 'El Sauce'),
(52, 'José Alfonso', 'García López', 46, '24624642-0', 'Santiago de Maria'),
(53, 'Norma Nelia', 'Figueroa Salmorán', 53, '42624642-0', 'Usulutan'),
(54, 'Jorge Alberto', 'Figueroa Valle', 77, '66843574-0', 'San Francisco Gotera'),
(55, 'Octavio Joel', 'Flores Díaz', 65, '24247257-0', 'San Salvador'),
(56, 'Alma Estela', 'Flores Martínez', 19, '41383272-0', 'Apopa'),
(57, 'Fernando', 'Flores Monroy', 22, '72472478-0', 'Soyapango'),
(58, 'Israel', 'Flores Rodríguez', 24, '35235232-0', 'Dulce nombre de Maria'),
(59, 'Lorenzo Valentín', 'Flores Salinas', 26, '24642644-0', 'El Transito'),
(60, 'Jorge de Jesús', 'Flores Sánchez', 28, '42634642-0', 'Apopa'),
(61, 'María Antonieta', 'Forment Hernández', 30, '25742574-0', 'San Miguel'),
(62, 'Ernesto', 'Franco Mota', 32, '67247485-0', 'Chinameca'),
(63, 'Erick', 'Fuentes Altamirano', 34, '53858353-0', 'Tecapan'),
(64, 'Omar ', 'Fuentes Cerdán', 40, '35738553-0', 'Tejutepeque'),
(65, 'Carlos Porfirio', 'Fuentes Mena', 42, '35757335-0', 'Victoria'),
(66, 'Mario Francisco', 'García Cancino', 44, '35735533-0', 'Jocoro'),
(67, 'Verónica Rachel', ' Martínez Flores', 48, '35735735-0', 'Perquin'),
(68, 'Jairo', 'Larreynaga', 23, '33399922-0', 'La Union'),
(69, 'Juan Manuel', 'Ramirez', 66, '22778880-0', 'Concepcion de Ataco'),
(70, 'Manolo', 'Pereira Martinez', 23, '02206666-0', 'El Refugio'),
(71, 'Clemencia', 'Paredes Huiza', 81, '02207777-0', 'Guaymango'),
(72, 'Catalina Venecia', 'Perez', 23, '00228888-0', 'Jujutla'),
(73, 'Clementina Ernesta', 'Posada', 24, '00446677-0', 'San Francisco Menendez'),
(74, 'Ernesto Francisco', 'Cordero', 19, '00333333-0', 'Tacuba'),
(75, 'Fernando', 'Colindres Castro', 45, '00440044--', 'Acajutla'),
(76, 'Fabian Roberto', 'Manzano', 77, '44444444-0', 'Armenia'),
(77, 'Ulises', 'Rodriguez Rodriguez', 19, '33339999-o', 'Caluco'),
(78, 'Catherine', 'Lisarra Perez', 21, '22222222-0', 'Izalco'),
(79, 'Rubin', 'Flores Pereira', 23, '00004545-0', 'Juayua'),
(80, 'Oscar', 'Rivas Rotan', 25, '99991111-0', 'Nahuizalco'),
(81, 'Oscar', 'Castro Rivas', 27, '22334444-0', 'Nahulingo'),
(82, 'Adalberto', 'Gomez Castro', 19, '11990033-9', 'Salcoatitan'),
(83, 'Cristian', '	Montano Baldizon', 38, '11112222-8', 'San Antonio del Monte'),
(84, 'Stanley', 'Cucura Manzana', 23, '22334423-4', 'San Julian'),
(85, 'Federizo', 'Zeledon Ferreira', 34, '09992222-1', 'Santa Catarina Masahuat'),
(86, 'Lourdes', 'Lara Lara', 43, '11223344-1', 'Sonsonate'),
(87, 'Raquel', 'Perez Lechuga', 18, '22334444-5', 'Huizucar'),
(88, 'Ulises', 'Tecala Perez', 21, '44556666-8', 'Jayaque'),
(89, 'Silvestre', 'Beniz Martinez', 55, '99009933-1', 'San Juan Opico'),
(90, 'Josue Carlos', 'Rivera', 23, '22332323-7', 'Quezaltepeque'),
(91, 'Juan Carlos', 'Silva', 26, '22334412-9', 'Sacacoyo'),
(92, 'Armando', 'Regalado Pereira', 19, '00887766-9', 'Talnique'),
(93, 'Irene', 'Guardado Perez', 45, '99888899-1', 'Aguilares'),
(94, 'Blanca', 'Hernandez Tobar', 22, '44554455-2', 'Ayutuxtepeque'),
(95, 'Gabriela', 'Perez Columbia', 27, '55443367-9', 'Ciudad Delgado'),
(96, 'Ashley Isela', 'Gonzales', 19, '23456712-9', 'Guazapa'),
(97, 'Roberto Carlos', 'Linares', 29, '99889823-8', 'Ilopango'),
(98, 'Alesandra', 'Marroquin Hernandez', 23, '12123434-8', 'El Paisnal'),
(99, 'Oscar', 'Robles Castro', 25, '09092323-8', 'Nejapa'),
(100, 'Rebeca Patricia', 'Castro', 19, '11223434-8', 'Panchimalco'),
(101, 'Lilian Michelle', 'Gonzales', 47, '99880909-1', 'San Marcos'),
(102, 'Fernando Oscar', 'Martinez', 89, '23234578-7', 'San Martin'),
(103, 'Jonathan', 'Siguanza Martinez', 67, '98987676-8', 'Chalchuapa'),
(104, 'Lilian', 'Castro de Martinez', 46, '88776655-6', 'El Porvenir'),
(105, 'Eugenio', 'Castro Castro', 67, '67678888-7', 'Masahuat'),
(106, 'Veronica', 'Perez Serrano', 77, '88998899-7', 'San Antonio Pajonal'),
(107, 'Alvaro Anibal', 'Castellano', 38, '88334455-7', 'Textitepeque'),
(108, 'Reina', 'Alvarado Valencia', 46, '66778888-6', 'Agua Caliente'),
(109, 'Alejandra Sofia', 'De Paz', 76, '11227777-6', 'Arcatao'),
(110, 'Francisco Armando', 'Valencia', 39, '22556666-5', 'Azacualpa'),
(111, 'Cristela', 'Alfonso Robles', 29, '99889999-6', 'Chalatenango'),
(112, 'Elsy', 'Artiga Alfonso', 56, '66778888-3', 'Citala'),
(113, 'Patricia', 'Ceron Martinez', 22, '11221111-4', 'Comalapa'),
(114, 'Cristina', 'Cañas Pasarela', 55, '22555544-3', 'El Carrizal'),
(115, 'Ana Patricia', 'Camacho', 96, '88000022-3', 'La Laguna'),
(116, 'Juan Miguel', 'Artiaga', 33, '33904444-5', 'Cojutepeque'),
(117, 'Diana Karina', 'Moncho', 33, '90884455-3', 'Monte San Juan'),
(118, 'Karina Raquel', 'Palacios', 27, '77222288-3', 'San Cristobal'),
(119, 'Karla', 'Velazques Robles', 33, '33556666-9', 'San Ramon'),
(120, 'Jose Luis', 'Contreeas', 34, '44554444-7', 'Tenancingo'),
(121, 'Alejandra Dayana', 'Palacios', 29, '99000000-3', 'Cuyultitan'),
(122, 'Andrea Beatriz', 'Marroquin', 34, '66777777-4', 'Olocuilta'),
(123, 'Martin', 'Caserio del Campo', 55, '99001122-2', 'Paraiso de Osorio'),
(124, 'Ileana', 'Garcia Castro', 22, '22444444-7', 'San Francisco Chinameca'),
(125, 'Lizbeth', 'Calleja Cebada', 33, '77777788-6', 'San Juan Nonualco'),
(126, 'Juana', 'Coronado Miguel', 34, '33443344-3', 'Zacatecoluca'),
(127, 'Anderson Joel', 'Aguilar', 44, '88770099-9', 'Apastepeque'),
(128, 'Juan', 'Osorio Español', 23, '33445566-4', 'San Sebastian'),
(129, 'Lola', 'Manzano de Hernandez', 77, '88998899-9', 'Dolores'),
(130, 'Carlos Enrique', 'Buzo', 66, '99889988-6', 'Jutiapa'),
(131, 'Carlos', 'Aguilar Pacheco', 34, '00990000-3', 'Sensuntepeque'),
(132, 'Miguel Guillermo', 'Del Toro', 55, '66666600-4', 'Tejutepeque'),
(133, 'Luisa', 'Magaña Ulin', 33, '33778888-8', 'Tecapan'),
(134, 'Luna Patricia', 'Magdaleno', 67, '77887777-8', 'Santiago de Maria'),
(135, 'Luis', 'Fonsi Peñate', 79, '44445555-3', 'Nueva Guadalupe'),
(136, 'Gerardo', 'Quintanilla Osicala', 66, '66666666-6', 'San Rafael Oriente'),
(137, 'Luis Fernando', 'Lopez', 56, '88888888-9', 'Osicala'),
(138, 'Ana Maria', 'Hernan', 33, '44444444-4', 'Corinto'),
(139, 'Alvaro', 'Cortez Peñate', 45, '00000033-3', 'San Isidro'),
(140, 'Clara Luz', 'Del Monte', 44, '55555578-8', 'Tecapan'),
(141, 'Roberto Adrian', 'Castaneda', 55, '11111111-2', 'Santa Clara'),
(142, 'Julio Cesar', 'Gomez', 45, '55557777-7', 'San Sebastian'),
(143, 'Eliza', 'Fuentes Perez', 66, '66666699-9', 'San Juan Talpa'),
(144, 'Elizabeth', 'Alfredo Perulapan', 33, '99993333-3', 'San Juan Nonualco'),
(145, 'Felix Daniel', 'Reyes', 56, '77777777-7', 'El Rosario de la Paz'),
(146, 'Pablo', 'Guzman Cruz', 36, '77888888-7', 'Culyutitan'),
(147, 'Marvin Felipe', 'Vivas', 44, '11222222-2', 'San Cristobal'),
(148, 'Enrique', 'Zepeda Solis', 66, '00001111-1', 'Comalapa'),
(149, 'Brenda', 'Martell Gilberto', 45, '77778888-2', 'La Laguna'),
(150, 'Eric', 'Marroquin Palacios', 49, '11221111-2', 'Tecoluca'),
(151, 'Roberto Antonio', 'Alcoverde Martínez', 33, '25768822-0', 'Colon');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_usuario`
--

CREATE TABLE IF NOT EXISTS `tipo_usuario` (
  `Id_TU` int(10) UNSIGNED NOT NULL,
  `Descripcion_TU` varchar(20) NOT NULL,
  PRIMARY KEY (`Id_TU`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `tipo_usuario`
--

INSERT INTO `tipo_usuario` (`Id_TU`, `Descripcion_TU`) VALUES
(1, 'Admin'),
(2, 'Usuario');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario`
--

CREATE TABLE IF NOT EXISTS `usuario` (
  `Id_Usuario` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `Password_Usuario` varchar(255) NOT NULL,
  `Id_TU` int(10) UNSIGNED NOT NULL,
  `Id_Persona` int(10) UNSIGNED NOT NULL,
  `Id_EVU` int(10) NOT NULL,
  PRIMARY KEY (`Id_Usuario`),
  UNIQUE KEY `Id_Persona` (`Id_Persona`),
  KEY `FK_Usuario_TU` (`Id_TU`),
  KEY `FK_Usuario_EVU` (`Id_EVU`),
  KEY `FK_Usuario_Persona` (`Id_Persona`)
) ENGINE=InnoDB AUTO_INCREMENT=160 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `usuario`
--

INSERT INTO `usuario` (`Id_Usuario`, `Password_Usuario`, `Id_TU`, `Id_Persona`, `Id_EVU`) VALUES
(2, '$2y$10$excIoDjAeuOhFgMh7qnb6efTE2bLAYC.RCNqGejkcUgwbRXQ.cEiu', 1, 2, 2),
(3, '$2y$10$excIoDjAeuOhFgMh7qnb6efTE2bLAYC.RCNqGejkcUgwbRXQ.cEiu', 2, 3, 2),
(8, '$2y$10$excIoDjAeuOhFgMh7qnb6efTE2bLAYC.RCNqGejkcUgwbRXQ.cEiu', 2, 1, 2),
(12, '$2y$10$excIoDjAeuOhFgMh7qnb6efTE2bLAYC.RCNqGejkcUgwbRXQ.cEiu', 1, 141, 2),
(13, '$2y$10$excIoDjAeuOhFgMh7qnb6efTE2bLAYC.RCNqGejkcUgwbRXQ.cEiu', 2, 4, 2),
(14, '$2y$10$excIoDjAeuOhFgMh7qnb6efTE2bLAYC.RCNqGejkcUgwbRXQ.cEiu', 2, 5, 2),
(15, '$2y$10$excIoDjAeuOhFgMh7qnb6efTE2bLAYC.RCNqGejkcUgwbRXQ.cEiu', 2, 6, 2),
(16, '$2y$10$excIoDjAeuOhFgMh7qnb6efTE2bLAYC.RCNqGejkcUgwbRXQ.cEiu', 2, 7, 2),
(17, '$2y$10$excIoDjAeuOhFgMh7qnb6efTE2bLAYC.RCNqGejkcUgwbRXQ.cEiu', 2, 8, 2),
(18, '$2y$10$excIoDjAeuOhFgMh7qnb6efTE2bLAYC.RCNqGejkcUgwbRXQ.cEiu', 2, 9, 2),
(19, '$2y$10$excIoDjAeuOhFgMh7qnb6efTE2bLAYC.RCNqGejkcUgwbRXQ.cEiu', 2, 10, 2),
(20, '$2y$10$excIoDjAeuOhFgMh7qnb6efTE2bLAYC.RCNqGejkcUgwbRXQ.cEiu', 2, 11, 2),
(21, '$2y$10$excIoDjAeuOhFgMh7qnb6efTE2bLAYC.RCNqGejkcUgwbRXQ.cEiu', 2, 12, 2),
(22, '$2y$10$excIoDjAeuOhFgMh7qnb6efTE2bLAYC.RCNqGejkcUgwbRXQ.cEiu', 2, 13, 2),
(23, '$2y$10$excIoDjAeuOhFgMh7qnb6efTE2bLAYC.RCNqGejkcUgwbRXQ.cEiu', 2, 14, 2),
(24, '$2y$10$excIoDjAeuOhFgMh7qnb6efTE2bLAYC.RCNqGejkcUgwbRXQ.cEiu', 2, 15, 2),
(25, '$2y$10$excIoDjAeuOhFgMh7qnb6efTE2bLAYC.RCNqGejkcUgwbRXQ.cEiu', 2, 16, 2),
(26, '$2y$10$excIoDjAeuOhFgMh7qnb6efTE2bLAYC.RCNqGejkcUgwbRXQ.cEiu', 2, 17, 2),
(27, '$2y$10$excIoDjAeuOhFgMh7qnb6efTE2bLAYC.RCNqGejkcUgwbRXQ.cEiu', 2, 18, 2),
(28, '$2y$10$excIoDjAeuOhFgMh7qnb6efTE2bLAYC.RCNqGejkcUgwbRXQ.cEiu', 2, 19, 2),
(29, '$2y$10$excIoDjAeuOhFgMh7qnb6efTE2bLAYC.RCNqGejkcUgwbRXQ.cEiu', 2, 20, 2),
(30, '$2y$10$excIoDjAeuOhFgMh7qnb6efTE2bLAYC.RCNqGejkcUgwbRXQ.cEiu', 2, 21, 2),
(31, '$2y$10$excIoDjAeuOhFgMh7qnb6efTE2bLAYC.RCNqGejkcUgwbRXQ.cEiu', 2, 22, 2),
(32, '$2y$10$excIoDjAeuOhFgMh7qnb6efTE2bLAYC.RCNqGejkcUgwbRXQ.cEiu', 2, 23, 2),
(33, '$2y$10$excIoDjAeuOhFgMh7qnb6efTE2bLAYC.RCNqGejkcUgwbRXQ.cEiu', 2, 24, 2),
(34, '$2y$10$excIoDjAeuOhFgMh7qnb6efTE2bLAYC.RCNqGejkcUgwbRXQ.cEiu', 2, 25, 2),
(35, '$2y$10$excIoDjAeuOhFgMh7qnb6efTE2bLAYC.RCNqGejkcUgwbRXQ.cEiu', 2, 26, 2),
(36, '$2y$10$excIoDjAeuOhFgMh7qnb6efTE2bLAYC.RCNqGejkcUgwbRXQ.cEiu', 2, 27, 2),
(37, '$2y$10$excIoDjAeuOhFgMh7qnb6efTE2bLAYC.RCNqGejkcUgwbRXQ.cEiu', 2, 28, 2),
(38, '$2y$10$excIoDjAeuOhFgMh7qnb6efTE2bLAYC.RCNqGejkcUgwbRXQ.cEiu', 2, 29, 2),
(39, '$2y$10$excIoDjAeuOhFgMh7qnb6efTE2bLAYC.RCNqGejkcUgwbRXQ.cEiu', 2, 30, 2),
(40, '$2y$10$excIoDjAeuOhFgMh7qnb6efTE2bLAYC.RCNqGejkcUgwbRXQ.cEiu', 2, 31, 2),
(41, '$2y$10$excIoDjAeuOhFgMh7qnb6efTE2bLAYC.RCNqGejkcUgwbRXQ.cEiu', 2, 32, 2),
(42, '$2y$10$excIoDjAeuOhFgMh7qnb6efTE2bLAYC.RCNqGejkcUgwbRXQ.cEiu', 2, 33, 2),
(43, '$2y$10$excIoDjAeuOhFgMh7qnb6efTE2bLAYC.RCNqGejkcUgwbRXQ.cEiu', 2, 34, 2),
(44, '$2y$10$excIoDjAeuOhFgMh7qnb6efTE2bLAYC.RCNqGejkcUgwbRXQ.cEiu', 2, 35, 2),
(45, '$2y$10$excIoDjAeuOhFgMh7qnb6efTE2bLAYC.RCNqGejkcUgwbRXQ.cEiu', 2, 36, 2),
(46, '$2y$10$excIoDjAeuOhFgMh7qnb6efTE2bLAYC.RCNqGejkcUgwbRXQ.cEiu', 2, 37, 2),
(47, '$2y$10$excIoDjAeuOhFgMh7qnb6efTE2bLAYC.RCNqGejkcUgwbRXQ.cEiu', 2, 38, 2),
(48, '$2y$10$excIoDjAeuOhFgMh7qnb6efTE2bLAYC.RCNqGejkcUgwbRXQ.cEiu', 2, 39, 2),
(49, '$2y$10$excIoDjAeuOhFgMh7qnb6efTE2bLAYC.RCNqGejkcUgwbRXQ.cEiu', 2, 40, 2),
(50, '$2y$10$excIoDjAeuOhFgMh7qnb6efTE2bLAYC.RCNqGejkcUgwbRXQ.cEiu', 2, 41, 2),
(51, '$2y$10$excIoDjAeuOhFgMh7qnb6efTE2bLAYC.RCNqGejkcUgwbRXQ.cEiu', 2, 42, 2),
(52, '$2y$10$excIoDjAeuOhFgMh7qnb6efTE2bLAYC.RCNqGejkcUgwbRXQ.cEiu', 2, 43, 2),
(53, '$2y$10$excIoDjAeuOhFgMh7qnb6efTE2bLAYC.RCNqGejkcUgwbRXQ.cEiu', 2, 44, 2),
(54, '$2y$10$excIoDjAeuOhFgMh7qnb6efTE2bLAYC.RCNqGejkcUgwbRXQ.cEiu', 2, 45, 2),
(55, '$2y$10$excIoDjAeuOhFgMh7qnb6efTE2bLAYC.RCNqGejkcUgwbRXQ.cEiu', 2, 46, 2),
(56, '$2y$10$excIoDjAeuOhFgMh7qnb6efTE2bLAYC.RCNqGejkcUgwbRXQ.cEiu', 2, 47, 2),
(57, '$2y$10$excIoDjAeuOhFgMh7qnb6efTE2bLAYC.RCNqGejkcUgwbRXQ.cEiu', 2, 48, 2),
(58, '$2y$10$excIoDjAeuOhFgMh7qnb6efTE2bLAYC.RCNqGejkcUgwbRXQ.cEiu', 2, 49, 2),
(59, '$2y$10$excIoDjAeuOhFgMh7qnb6efTE2bLAYC.RCNqGejkcUgwbRXQ.cEiu', 2, 50, 2),
(60, '$2y$10$excIoDjAeuOhFgMh7qnb6efTE2bLAYC.RCNqGejkcUgwbRXQ.cEiu', 2, 51, 2),
(61, '$2y$10$excIoDjAeuOhFgMh7qnb6efTE2bLAYC.RCNqGejkcUgwbRXQ.cEiu', 2, 52, 2),
(62, '$2y$10$excIoDjAeuOhFgMh7qnb6efTE2bLAYC.RCNqGejkcUgwbRXQ.cEiu', 2, 53, 2),
(63, '$2y$10$excIoDjAeuOhFgMh7qnb6efTE2bLAYC.RCNqGejkcUgwbRXQ.cEiu', 2, 54, 2),
(64, '$2y$10$excIoDjAeuOhFgMh7qnb6efTE2bLAYC.RCNqGejkcUgwbRXQ.cEiu', 2, 55, 2),
(65, '$2y$10$excIoDjAeuOhFgMh7qnb6efTE2bLAYC.RCNqGejkcUgwbRXQ.cEiu', 2, 56, 2),
(66, '$2y$10$excIoDjAeuOhFgMh7qnb6efTE2bLAYC.RCNqGejkcUgwbRXQ.cEiu', 2, 57, 2),
(67, '$2y$10$excIoDjAeuOhFgMh7qnb6efTE2bLAYC.RCNqGejkcUgwbRXQ.cEiu', 2, 58, 2),
(68, '$2y$10$excIoDjAeuOhFgMh7qnb6efTE2bLAYC.RCNqGejkcUgwbRXQ.cEiu', 2, 59, 2),
(69, '$2y$10$excIoDjAeuOhFgMh7qnb6efTE2bLAYC.RCNqGejkcUgwbRXQ.cEiu', 2, 60, 2),
(70, '$2y$10$excIoDjAeuOhFgMh7qnb6efTE2bLAYC.RCNqGejkcUgwbRXQ.cEiu', 2, 61, 2),
(71, '$2y$10$excIoDjAeuOhFgMh7qnb6efTE2bLAYC.RCNqGejkcUgwbRXQ.cEiu', 2, 62, 2),
(72, '$2y$10$excIoDjAeuOhFgMh7qnb6efTE2bLAYC.RCNqGejkcUgwbRXQ.cEiu', 2, 63, 2),
(73, '$2y$10$excIoDjAeuOhFgMh7qnb6efTE2bLAYC.RCNqGejkcUgwbRXQ.cEiu', 2, 64, 2),
(74, '$2y$10$excIoDjAeuOhFgMh7qnb6efTE2bLAYC.RCNqGejkcUgwbRXQ.cEiu', 2, 65, 2),
(75, '$2y$10$excIoDjAeuOhFgMh7qnb6efTE2bLAYC.RCNqGejkcUgwbRXQ.cEiu', 2, 66, 2),
(76, '$2y$10$excIoDjAeuOhFgMh7qnb6efTE2bLAYC.RCNqGejkcUgwbRXQ.cEiu', 2, 67, 2),
(77, '$2y$10$excIoDjAeuOhFgMh7qnb6efTE2bLAYC.RCNqGejkcUgwbRXQ.cEiu', 2, 68, 2),
(78, '$2y$10$excIoDjAeuOhFgMh7qnb6efTE2bLAYC.RCNqGejkcUgwbRXQ.cEiu', 2, 69, 2),
(79, '$2y$10$excIoDjAeuOhFgMh7qnb6efTE2bLAYC.RCNqGejkcUgwbRXQ.cEiu', 2, 70, 2),
(80, '$2y$10$excIoDjAeuOhFgMh7qnb6efTE2bLAYC.RCNqGejkcUgwbRXQ.cEiu', 2, 71, 2),
(81, '$2y$10$excIoDjAeuOhFgMh7qnb6efTE2bLAYC.RCNqGejkcUgwbRXQ.cEiu', 2, 72, 2),
(82, '$2y$10$excIoDjAeuOhFgMh7qnb6efTE2bLAYC.RCNqGejkcUgwbRXQ.cEiu', 2, 73, 2),
(83, '$2y$10$excIoDjAeuOhFgMh7qnb6efTE2bLAYC.RCNqGejkcUgwbRXQ.cEiu', 2, 74, 2),
(84, '$2y$10$excIoDjAeuOhFgMh7qnb6efTE2bLAYC.RCNqGejkcUgwbRXQ.cEiu', 2, 75, 2),
(85, '$2y$10$excIoDjAeuOhFgMh7qnb6efTE2bLAYC.RCNqGejkcUgwbRXQ.cEiu', 2, 76, 2),
(86, '$2y$10$excIoDjAeuOhFgMh7qnb6efTE2bLAYC.RCNqGejkcUgwbRXQ.cEiu', 2, 77, 2),
(87, '$2y$10$excIoDjAeuOhFgMh7qnb6efTE2bLAYC.RCNqGejkcUgwbRXQ.cEiu', 2, 78, 2),
(88, '$2y$10$excIoDjAeuOhFgMh7qnb6efTE2bLAYC.RCNqGejkcUgwbRXQ.cEiu', 2, 79, 2),
(89, '$2y$10$excIoDjAeuOhFgMh7qnb6efTE2bLAYC.RCNqGejkcUgwbRXQ.cEiu', 2, 80, 2),
(90, '$2y$10$excIoDjAeuOhFgMh7qnb6efTE2bLAYC.RCNqGejkcUgwbRXQ.cEiu', 2, 81, 2),
(91, '$2y$10$excIoDjAeuOhFgMh7qnb6efTE2bLAYC.RCNqGejkcUgwbRXQ.cEiu', 2, 82, 2),
(92, '$2y$10$excIoDjAeuOhFgMh7qnb6efTE2bLAYC.RCNqGejkcUgwbRXQ.cEiu', 2, 83, 2),
(93, '$2y$10$excIoDjAeuOhFgMh7qnb6efTE2bLAYC.RCNqGejkcUgwbRXQ.cEiu', 2, 84, 2),
(94, '$2y$10$excIoDjAeuOhFgMh7qnb6efTE2bLAYC.RCNqGejkcUgwbRXQ.cEiu', 2, 85, 2),
(95, '$2y$10$excIoDjAeuOhFgMh7qnb6efTE2bLAYC.RCNqGejkcUgwbRXQ.cEiu', 2, 86, 2),
(96, '$2y$10$excIoDjAeuOhFgMh7qnb6efTE2bLAYC.RCNqGejkcUgwbRXQ.cEiu', 2, 87, 2),
(97, '$2y$10$excIoDjAeuOhFgMh7qnb6efTE2bLAYC.RCNqGejkcUgwbRXQ.cEiu', 2, 88, 2),
(98, '$2y$10$excIoDjAeuOhFgMh7qnb6efTE2bLAYC.RCNqGejkcUgwbRXQ.cEiu', 2, 89, 2),
(99, '$2y$10$excIoDjAeuOhFgMh7qnb6efTE2bLAYC.RCNqGejkcUgwbRXQ.cEiu', 2, 90, 2),
(100, '$2y$10$excIoDjAeuOhFgMh7qnb6efTE2bLAYC.RCNqGejkcUgwbRXQ.cEiu', 2, 91, 2),
(101, '$2y$10$excIoDjAeuOhFgMh7qnb6efTE2bLAYC.RCNqGejkcUgwbRXQ.cEiu', 2, 92, 2),
(102, '$2y$10$excIoDjAeuOhFgMh7qnb6efTE2bLAYC.RCNqGejkcUgwbRXQ.cEiu', 2, 93, 2),
(103, '$2y$10$excIoDjAeuOhFgMh7qnb6efTE2bLAYC.RCNqGejkcUgwbRXQ.cEiu', 2, 94, 2),
(104, '$2y$10$excIoDjAeuOhFgMh7qnb6efTE2bLAYC.RCNqGejkcUgwbRXQ.cEiu', 2, 95, 2),
(105, '$2y$10$excIoDjAeuOhFgMh7qnb6efTE2bLAYC.RCNqGejkcUgwbRXQ.cEiu', 2, 96, 2),
(106, '$2y$10$excIoDjAeuOhFgMh7qnb6efTE2bLAYC.RCNqGejkcUgwbRXQ.cEiu', 2, 97, 2),
(107, '$2y$10$excIoDjAeuOhFgMh7qnb6efTE2bLAYC.RCNqGejkcUgwbRXQ.cEiu', 2, 98, 2),
(108, '$2y$10$excIoDjAeuOhFgMh7qnb6efTE2bLAYC.RCNqGejkcUgwbRXQ.cEiu', 2, 99, 2),
(109, '$2y$10$excIoDjAeuOhFgMh7qnb6efTE2bLAYC.RCNqGejkcUgwbRXQ.cEiu', 2, 100, 2),
(110, '$2y$10$excIoDjAeuOhFgMh7qnb6efTE2bLAYC.RCNqGejkcUgwbRXQ.cEiu', 2, 101, 2),
(111, '$2y$10$excIoDjAeuOhFgMh7qnb6efTE2bLAYC.RCNqGejkcUgwbRXQ.cEiu', 2, 102, 2),
(112, '$2y$10$excIoDjAeuOhFgMh7qnb6efTE2bLAYC.RCNqGejkcUgwbRXQ.cEiu', 2, 103, 2),
(113, '$2y$10$excIoDjAeuOhFgMh7qnb6efTE2bLAYC.RCNqGejkcUgwbRXQ.cEiu', 2, 104, 2),
(114, '$2y$10$excIoDjAeuOhFgMh7qnb6efTE2bLAYC.RCNqGejkcUgwbRXQ.cEiu', 2, 105, 2),
(115, '$2y$10$excIoDjAeuOhFgMh7qnb6efTE2bLAYC.RCNqGejkcUgwbRXQ.cEiu', 2, 106, 2),
(116, '$2y$10$excIoDjAeuOhFgMh7qnb6efTE2bLAYC.RCNqGejkcUgwbRXQ.cEiu', 2, 107, 2),
(117, '$2y$10$excIoDjAeuOhFgMh7qnb6efTE2bLAYC.RCNqGejkcUgwbRXQ.cEiu', 2, 108, 2),
(118, '$2y$10$excIoDjAeuOhFgMh7qnb6efTE2bLAYC.RCNqGejkcUgwbRXQ.cEiu', 2, 109, 2),
(119, '$2y$10$excIoDjAeuOhFgMh7qnb6efTE2bLAYC.RCNqGejkcUgwbRXQ.cEiu', 2, 110, 2),
(120, '$2y$10$excIoDjAeuOhFgMh7qnb6efTE2bLAYC.RCNqGejkcUgwbRXQ.cEiu', 2, 111, 2),
(121, '$2y$10$excIoDjAeuOhFgMh7qnb6efTE2bLAYC.RCNqGejkcUgwbRXQ.cEiu', 2, 112, 2),
(122, '$2y$10$excIoDjAeuOhFgMh7qnb6efTE2bLAYC.RCNqGejkcUgwbRXQ.cEiu', 2, 113, 2),
(123, '$2y$10$excIoDjAeuOhFgMh7qnb6efTE2bLAYC.RCNqGejkcUgwbRXQ.cEiu', 2, 114, 2),
(124, '$2y$10$excIoDjAeuOhFgMh7qnb6efTE2bLAYC.RCNqGejkcUgwbRXQ.cEiu', 2, 115, 2),
(125, '$2y$10$excIoDjAeuOhFgMh7qnb6efTE2bLAYC.RCNqGejkcUgwbRXQ.cEiu', 2, 116, 2),
(126, '$2y$10$excIoDjAeuOhFgMh7qnb6efTE2bLAYC.RCNqGejkcUgwbRXQ.cEiu', 2, 117, 2),
(127, '$2y$10$excIoDjAeuOhFgMh7qnb6efTE2bLAYC.RCNqGejkcUgwbRXQ.cEiu', 2, 118, 2),
(128, '$2y$10$excIoDjAeuOhFgMh7qnb6efTE2bLAYC.RCNqGejkcUgwbRXQ.cEiu', 2, 119, 2),
(129, '$2y$10$excIoDjAeuOhFgMh7qnb6efTE2bLAYC.RCNqGejkcUgwbRXQ.cEiu', 2, 120, 2),
(130, '$2y$10$excIoDjAeuOhFgMh7qnb6efTE2bLAYC.RCNqGejkcUgwbRXQ.cEiu', 2, 121, 2),
(131, '$2y$10$excIoDjAeuOhFgMh7qnb6efTE2bLAYC.RCNqGejkcUgwbRXQ.cEiu', 2, 122, 2),
(132, '$2y$10$excIoDjAeuOhFgMh7qnb6efTE2bLAYC.RCNqGejkcUgwbRXQ.cEiu', 2, 123, 2),
(133, '$2y$10$excIoDjAeuOhFgMh7qnb6efTE2bLAYC.RCNqGejkcUgwbRXQ.cEiu', 2, 124, 2),
(134, '$2y$10$excIoDjAeuOhFgMh7qnb6efTE2bLAYC.RCNqGejkcUgwbRXQ.cEiu', 2, 125, 2),
(135, '$2y$10$excIoDjAeuOhFgMh7qnb6efTE2bLAYC.RCNqGejkcUgwbRXQ.cEiu', 2, 126, 2),
(136, '$2y$10$excIoDjAeuOhFgMh7qnb6efTE2bLAYC.RCNqGejkcUgwbRXQ.cEiu', 2, 127, 2),
(137, '$2y$10$excIoDjAeuOhFgMh7qnb6efTE2bLAYC.RCNqGejkcUgwbRXQ.cEiu', 2, 128, 2),
(138, '$2y$10$excIoDjAeuOhFgMh7qnb6efTE2bLAYC.RCNqGejkcUgwbRXQ.cEiu', 2, 129, 2),
(139, '$2y$10$excIoDjAeuOhFgMh7qnb6efTE2bLAYC.RCNqGejkcUgwbRXQ.cEiu', 2, 130, 2),
(140, '$2y$10$excIoDjAeuOhFgMh7qnb6efTE2bLAYC.RCNqGejkcUgwbRXQ.cEiu', 2, 131, 2),
(141, '$2y$10$excIoDjAeuOhFgMh7qnb6efTE2bLAYC.RCNqGejkcUgwbRXQ.cEiu', 2, 132, 2),
(142, '$2y$10$excIoDjAeuOhFgMh7qnb6efTE2bLAYC.RCNqGejkcUgwbRXQ.cEiu', 2, 133, 2),
(143, '$2y$10$excIoDjAeuOhFgMh7qnb6efTE2bLAYC.RCNqGejkcUgwbRXQ.cEiu', 2, 134, 2),
(144, '$2y$10$excIoDjAeuOhFgMh7qnb6efTE2bLAYC.RCNqGejkcUgwbRXQ.cEiu', 2, 135, 2),
(145, '$2y$10$excIoDjAeuOhFgMh7qnb6efTE2bLAYC.RCNqGejkcUgwbRXQ.cEiu', 2, 136, 2),
(146, '$2y$10$excIoDjAeuOhFgMh7qnb6efTE2bLAYC.RCNqGejkcUgwbRXQ.cEiu', 2, 137, 2),
(147, '$2y$10$excIoDjAeuOhFgMh7qnb6efTE2bLAYC.RCNqGejkcUgwbRXQ.cEiu', 2, 138, 2),
(148, '$2y$10$excIoDjAeuOhFgMh7qnb6efTE2bLAYC.RCNqGejkcUgwbRXQ.cEiu', 2, 139, 2),
(149, '$2y$10$excIoDjAeuOhFgMh7qnb6efTE2bLAYC.RCNqGejkcUgwbRXQ.cEiu', 2, 140, 2),
(150, '$2y$10$excIoDjAeuOhFgMh7qnb6efTE2bLAYC.RCNqGejkcUgwbRXQ.cEiu', 2, 142, 2),
(151, '$2y$10$excIoDjAeuOhFgMh7qnb6efTE2bLAYC.RCNqGejkcUgwbRXQ.cEiu', 2, 143, 2),
(152, '$2y$10$excIoDjAeuOhFgMh7qnb6efTE2bLAYC.RCNqGejkcUgwbRXQ.cEiu', 2, 144, 2),
(153, '$2y$10$excIoDjAeuOhFgMh7qnb6efTE2bLAYC.RCNqGejkcUgwbRXQ.cEiu', 2, 145, 2),
(154, '$2y$10$excIoDjAeuOhFgMh7qnb6efTE2bLAYC.RCNqGejkcUgwbRXQ.cEiu', 2, 146, 2),
(155, '$2y$10$excIoDjAeuOhFgMh7qnb6efTE2bLAYC.RCNqGejkcUgwbRXQ.cEiu', 2, 147, 2),
(156, '$2y$10$excIoDjAeuOhFgMh7qnb6efTE2bLAYC.RCNqGejkcUgwbRXQ.cEiu', 2, 148, 2),
(157, '$2y$10$excIoDjAeuOhFgMh7qnb6efTE2bLAYC.RCNqGejkcUgwbRXQ.cEiu', 2, 149, 2),
(158, '$2y$10$excIoDjAeuOhFgMh7qnb6efTE2bLAYC.RCNqGejkcUgwbRXQ.cEiu', 2, 150, 2),
(159, '$2y$10$excIoDjAeuOhFgMh7qnb6efTE2bLAYC.RCNqGejkcUgwbRXQ.cEiu', 2, 151, 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `voto`
--

CREATE TABLE IF NOT EXISTS `voto` (
  `Id_Voto` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `Id_Candidato` int(10) UNSIGNED NOT NULL,
  `Id_JRV` int(10) UNSIGNED NOT NULL,
  PRIMARY KEY (`Id_Voto`),
  KEY `Id_Candidato` (`Id_Candidato`),
  KEY `Id_JRV` (`Id_JRV`)
) ENGINE=InnoDB AUTO_INCREMENT=136 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `voto`
--

INSERT INTO `voto` (`Id_Voto`, `Id_Candidato`, `Id_JRV`) VALUES
(1, 19, 1),
(2, 21, 2),
(3, 24, 3),
(4, 25, 4),
(5, 19, 5),
(6, 19, 6),
(7, 19, 7),
(8, 24, 8),
(9, 25, 9),
(10, 21, 10),
(11, 21, 11),
(12, 21, 12),
(13, 21, 13),
(14, 21, 14),
(15, 19, 15),
(16, 19, 16),
(17, 19, 17),
(18, 19, 18),
(19, 24, 19),
(20, 24, 20),
(21, 21, 21),
(22, 21, 22),
(23, 21, 23),
(24, 21, 24),
(25, 21, 25),
(26, 19, 26),
(27, 19, 27),
(28, 19, 28),
(29, 19, 29),
(30, 19, 30),
(31, 19, 31),
(32, 19, 32),
(33, 21, 33),
(34, 21, 34),
(35, 24, 35),
(36, 25, 36),
(37, 25, 37),
(38, 25, 38),
(39, 25, 39),
(40, 24, 40),
(41, 24, 41),
(42, 21, 42),
(43, 19, 43),
(44, 19, 44),
(45, 19, 45),
(46, 19, 46),
(47, 21, 47),
(48, 21, 48),
(49, 21, 49),
(50, 21, 50),
(51, 19, 51),
(52, 19, 52),
(53, 19, 53),
(54, 19, 54),
(55, 19, 55),
(56, 19, 56),
(57, 19, 57),
(58, 21, 58),
(59, 21, 59),
(60, 24, 60),
(61, 24, 61),
(62, 24, 62),
(63, 24, 63),
(64, 21, 64),
(65, 21, 65),
(66, 21, 66),
(67, 21, 67),
(68, 21, 68),
(69, 21, 69),
(70, 19, 70),
(71, 21, 71),
(72, 19, 72),
(73, 19, 73),
(74, 21, 74),
(75, 21, 75),
(76, 21, 76),
(77, 24, 77),
(78, 25, 78),
(79, 25, 79),
(80, 24, 80),
(81, 24, 81),
(82, 21, 82),
(83, 21, 83),
(84, 21, 84),
(85, 19, 85),
(86, 19, 86),
(87, 19, 87),
(88, 21, 88),
(89, 21, 89),
(90, 21, 90),
(91, 19, 91),
(92, 21, 92),
(93, 21, 93),
(94, 21, 94),
(95, 24, 95),
(96, 24, 96),
(97, 24, 97),
(98, 25, 98),
(99, 21, 99),
(100, 19, 100),
(101, 19, 101),
(102, 19, 102),
(103, 19, 103),
(104, 21, 104),
(105, 24, 105),
(106, 25, 106),
(107, 19, 107),
(108, 21, 108),
(109, 21, 109),
(110, 24, 110),
(111, 25, 111),
(112, 21, 112),
(113, 25, 113),
(114, 25, 114),
(115, 21, 115),
(116, 24, 116),
(117, 24, 117),
(118, 25, 118),
(119, 25, 119),
(120, 24, 120),
(121, 21, 121),
(122, 19, 122),
(123, 21, 123),
(124, 25, 124),
(125, 24, 125),
(126, 24, 126),
(127, 21, 127),
(128, 21, 128),
(129, 25, 129),
(130, 19, 130),
(131, 21, 131),
(132, 24, 132),
(133, 21, 133),
(134, 25, 134),
(135, 25, 135);

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `voto`
--
ALTER TABLE `voto`
  ADD CONSTRAINT `voto_ibfk_1` FOREIGN KEY (`Id_JRV`) REFERENCES `jrv` (`Id_JRV`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
