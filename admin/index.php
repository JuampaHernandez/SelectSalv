<?php
	session_start();
	if (isset($_SESSION['usuario'])) 
	{
		if ($_SESSION['usuario']['tipo_Usuario'] == 2) 
		{
			header('Location: ../votar/');
		}
		else
		{
?>
<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<!-- Bootstrap css -->
	<link rel="stylesheet" href="../css/bootstrap.css">
	<link rel="stylesheet" href="../css/style.css">
	<!-- Icono de la página -->
	<link rel="icon" type="image/ico" href="../img/icon.ico">
	<!-- Scripts -->
	<script src="../js/jquery-3.2.1.min.js"></script>
	<script src="../js/popper.min.js"></script>
	<script src="../js/bootstrap.js"></script>
	<script src="../js/fontawesome-all.js"></script>
	<title>Administrador</title>
</head>
<body>
	<nav class="navbar navbar-expand-lg navbar-dark bg-primary">
		<h3 class="navbar-brand" style="margin: auto 1rem auto 0;">
			<img src="../img/email-icon.png" width="30" height="30" class="d-inline-block align-top" alt="SelectSalv">
			SelectSalv
		</h3>
		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>
		<div class="collapse navbar-collapse" id="navbarSupportedContent" style="text-align: center;">
			<ul class="navbar-nav mr-auto">
				<li class="nav-item active">
					<a class="nav-link" href="index.php"><span class="fas fa-home"></span> Inicio</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="actions/charts/"><span class="fas fa-chart-line"></span> Estadísticas</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="actions/usuarios/"><span class="fas fa-user-circle"></span> Usuarios</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="actions/candidatos/"><span class="fas fa-users"></span> Candidatos</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="actions/partidos/"><span class="fas fa-flag"></span> Partidos</a>
				</li>
				<li class="nav-item dropdown">
					<a class="nav-link dropdown-toggle" href="" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						Más
					</a>
					<div class="dropdown-menu" aria-labelledby="navbarDropdown">
						<a class="dropdown-item" href="actions/cv/"><span class="fas fa-map-marker-alt"></span> Centros de votación</a>
						<a class="dropdown-item" href="actions/jrv/"><span class="fas fa-archive"></span> Juntas receptoras</a>
						<a class="dropdown-item" href="actions/votar/"><span class="fas fa-address-card"></span> Votar</a>
					</div>
				</li>
			</ul>
			<form class="form-inline my-2 my-lg-0" align='center' action="../php/Logout.php">
			  <button class="btn btn-outline-light my-2 my-sm-0" type="submit" style="margin: auto;"><li class="fas fa-sign-out-alt"></li> Cerrar sessión</button>
			</form>
		</div>
	</nav>
	<div class="container">
		<div class="jumbotron jumbotron-fluid" style="margin-top: 2rem;">
			<div class="container">
				<h1 class="display-4">Bienvenido a la página administrador, <?php echo $_SESSION['usuario']['nombreUsuario']; ?>.</h1>
				<p class="lead">Vea estadísticas y gestione todo lo relacionado al proceso de elecciones. Mostrar, agregar, editar y eliminar alcaldes, partidos, centros de votación, juntas receptoras y más.</p>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-6">
				<div class="card" style="margin-bottom: 2rem;">
					<div class="card-body">
						<h5 class="card-title">Estadísticas</h5>
						<p class="card-text">Ver estadísticas por departamento y municipios.</p>
						<a href="" class="btn btn-info"><i class="fas fa-chart-line"></i> Ver estadísticas</a>
					</div>
				</div>
			</div>
			<div class="col-sm-6">
				<div class="card" style="margin-bottom: 2rem;">
					<div class="card-body">
						<h5 class="card-title">Partidos políticos</h5>
						<p class="card-text">Ver, agregar, editar y eliminar partidos políticos.</p>
						<a href="actions/partidos/" class="btn btn-info"><i class="fa fa-cog" aria-hidden="true"></i> Gestionar</a>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-6">
				<div class="card" style="margin-bottom: 2rem;">
				<div class="card-body">
					<h5 class="card-title">Candidatos</h5>
					<p class="card-text">Ver, agregar, editar y eliminar canditados a la presidencia.</p>
					<a href="actions/candidatos/" class="btn btn-info"><i class="fa fa-cog" aria-hidden="true"></i> Gestionar</a>
				</div>
				</div>
			</div>
			<div class="col-sm-6">
				<div class="card" style="margin-bottom: 2rem;">
					<div class="card-body">
						<h5 class="card-title">Usuarios</h5>
						<p class="card-text">Ver, agregar, editar y eliminar usuarios.</p>
						<a href="actions/usuarios/" class="btn btn-info"><i class="fa fa-cog" aria-hidden="true"></i> Gestionar</a>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-6">
				<div class="card" style="margin-bottom: 2rem;">
					<div class="card-body">
						<h5 class="card-title">Centros de votación</h5>
						<p class="card-text">Ver, agregar, editar y eliminar centros de votación.</p>
						<a href="actions/cv/" class="btn btn-info"><i class="fa fa-cog" aria-hidden="true"></i> Gestionar</a>
					</div>
				</div>
			</div>
			<div class="col-sm-6">
				<div class="card" style="margin-bottom: 2rem;">
					<div class="card-body">
						<h5 class="card-title">Juntas receptoras</h5>
						<p class="card-text">Ver, agregar, editar y eliminar juntas receptoras de votos.</p>
						<a href="actions/jrv/" class="btn btn-info"><i class="fa fa-cog" aria-hidden="true"></i> Gestionar</a>
					</div>
				</div>
			</div>
		</div>
	</div>
	<footer style="padding: 1rem;">
		<p style="text-align: center;">
			15 Calle poniente No. 4223, Colonia Escalón
			<br>
			San Salvador, El Salvador, C.A.
			<br>
			Conmutador: (503) 2209-4000
		</p>
		<div style="margin: 1rem auto;" align="center">
				<img src="../img/escudo.png" alt="Escudo" style="width: 5rem;">
		</div>
		<p style="text-align: center;">Selectsalv &copy; <?php echo date('Y'); ?> Todos los derechos reservados.</p>
	</footer>
</body>
</html>
<?php
		}
	}
	else
	{
		header('Location: ../login/');
	}
?>