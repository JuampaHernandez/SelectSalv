$(document).ready(function() {
	
	// Quitando la clase 'error' de los inputs en el evento 'Focus'
	$('.login-form input[type="text"], .login-form input[type="password"]').on('focus', function() {
		$(this).removeClass('is-invalid');
	});

	// Enviando datos
	$(document).on('click', '#btnLogin', function(event) {
		event.preventDefault();
		if (validarLogin()) {
			var tmpDui = $('#txtUsuario').val();
			tmpDui.replace("/'/g", "''");
			var datos = {
				dui: tmpDui,
				password: $('#txtPass').val(),
				op: 'verificar',
				obj: 'usuario'
			};
			$.ajax({
				url: '../php/controllers/Controller.php',
				type: 'POST',
				data: datos,
				beforeSend: function() {
					$('#btnLogin').val('Validando...');
				},
				success: function(Resp) {
					// console.log(Resp);
					var res = JSON.parse(Resp);
					if (!res.error) {
						if (res.tipo == 1)
							location.href = '../admin/';
						else if (res.tipo == 2) {
							if (res.estado == 1)
								location.href = '../votar/';
							else
								mostrarError('Su usuario está deshabilitado.');
						}
					} else if (res.error)
						mostrarError('Datos incorrectos, intentelo de nuevo.');
					$('#btnLogin').val('Iniciar sesión');
				}
			});
			
		}
	});
});

function validarLogin() {
	var estado = true, tmp;
	$('.required').each(function() {
		tmp = $(this).val();
		if (tmp == "" || tmp == " ") {
			$(this).addClass('is-invalid');
			estado = false;
		}
	});
	return estado;
}

function mostrarError(error) {
	$('.alert').html(error);
	$('.alert').slideDown('slow');
	setTimeout(function(){
		$('.alert').slideUp('slow');
	}, 4000);
}