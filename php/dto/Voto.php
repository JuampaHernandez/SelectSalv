<?php
/**
 * Nombre de la clase: Voto
 * @version 1.0
 * @author Juan Pablo Elias Hernández
 */
class Voto
{
    /**
     * Atributo 'ID de candidato'
     * @var int
     */
    private $idCandidato;

    /**
     * Atributo 'ID de junta receptora de votos'
     * @var int
     */
    private $idJRV;

    public function __set($name, $value)
    {
        $this->$name = $value;
    }

    public function __get($name)
    {
        return $this->$name;
    }

    /**
     * Constructor de la clase Jrv
     */
    public function __construct()
    {
        $this->idCandidato = 0;
        $this->idJRV = 0;
    }
}
