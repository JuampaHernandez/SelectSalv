<?php
	/**
    * Nombre de la clase: Jrv
    * @version 1.0
	* @author Juan Pablo Elias Hernández
	*/
	class Jrv
	{
        /**
        * Atributo 'ID Junta Receptora de Votos'
        * @var int
        */
        private $id;

        /**
        * Atributo 'ID Centro de Votación'
        * @var int
        */
		private $idCV;

		/**
		* Atributo 'Nombre de Centro de Votación'
		* @var string
		*/
		private $nombreCV;

        /**
        * Atributo 'Número de Junta Receptora de Votos'
        * @var int
        */
        private $numero;

		public function __set($name, $value)
		{
			$this->$name = $value; 
		}

		public function __get($name)
		{
			return $this->$name;
		}

		/**
        * Constructor de la clase Jrv
		*/
		public function __construct()
		{
			$this->id = 0;
			$this->idCV = 0;
			$this->numero = 0;
			$this->nombreCV = 'Centro de votación';
		}
	}
?>